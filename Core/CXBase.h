#ifndef _GOAP_XBASE_H_
#define _GOAP_XBASE_H_

#include <utils-cpp/XBit.h>
#include <utils-cpp/Base.h>
#include <utils-cpp/XComponentBase.h>
#include <utils-cpp/XDelegate.h>

#include "../Action/AXBase.h"
#include "../Action/AXState.h"
#include "../Property/PXHandler.h"
#include "../Property/PXManager.h"
#include "../Property/PXSet.h"

#include "../Balance/Action/BAXNode.h"
#include "../Balance/Action/BAXSet.h"

#include "CIBase.h"
#include "CXBlackBoard.h"

namespace GOAP
{
	namespace Core
	{	
		///<summary>
		/// Class finds action plan that satisfies goal state based on current state
		///</summary>
		T(Property) class XBase : public IBase
		{
		protected:

			typedef Utils::Delegate::X4<void, bool&, int&, int, Action::XBase<TProperty> *> TActionCostDelegate;

		public:

			typedef Utils::Delegate::X2<void, int&, int& > TProceduralCostStrategyDelegate;

			//					//
			// Initialization	//
			//					//

			XBase()
			{
				m_pPlanner = NULL;
				m_pHandler = NULL;
				m_pManager = NULL;
				m_pActions = NULL;
				m_pBalance = NULL;

				m_pManager = BlackBoard.Get<Action::XManager>();

				m_pAction = new Action::XState<TProperty>();
				m_pAction->Initialize(NULL);

				m_uActionID = 0;
				m_uActionsMask = 0;

				m_hActionCost[0] = TActionCostDelegate::FromMethod < XBase, &XBase::NormalActionCost >(this);
				m_hActionCost[1] = TActionCostDelegate::FromMethod < XBase, &XBase::NormalActionCost >(this);

				DisableBalancer();
				SetDefaultProceduralCostStrategy();
			}

			~XBase()
			{
				delete m_pAction;

				m_pAction = NULL;

				DisableBalancer();
			}

			//								//
			// Component Interface Methods	//
			//								//

			void Initialize(Utils::Component::XBase *entity)
			{
				entity->RegisterMethod<XBase, &XBase::OnInitialize >("OnInitialize", this);
			}

			void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
			{
				m_pPlanner = entity->Require < Utils::Planner::IBase >();
				m_pHandler = entity->Require < Property::IHandler, Property::XHandler < TProperty > >();

				if (m_pHandler != NULL)
				{
					m_pHandler->InitializeAction(m_pAction);
				}
			}

			void Update(Utils::Component::XBase *entity)
			{
				m_pBalance = entity->Get < Balance::Action::XSet >();
				m_pActions = entity->Get < Action::XSet >();

				EnableBalancer();

				if (m_pActions != 0)
				{
					m_uActionsMask = Utils::Bit::GetCount(m_pActions->GetSize());
				}
				else
				{
					m_uActionsMask = 0;
				}
			}

			void Shutdown(Utils::Component::XBase *entity)
			{
				m_pPlanner = NULL;
				m_pHandler = NULL;
				m_pBalance = NULL;
				m_pActions = NULL;

				DisableBalancer();
			}

			//																			//
			// Utils Planner Data provider methods, called by planner as data provided	//
			//																			//


			///<summary>
			/// Set current working uNode
			///</summary>
			void UpdateCurrentNode(unsigned int uNode, unsigned int uSuperNode) 
			{
				if (uNode == m_uActionID)
				{
					return;
				}

				if (uNode > 0)
				{
					m_hStates.push_back(GetActionProperty(uNode, false));
				}

				m_uActionID = uNode;
			}

			void Reset()
			{
				count = 0;
				costs.clear();
				nodes.clear();
			}

			///<summary>
			/// Calculation Heuristics score between two nodes
			///</summary>
			int CalculateH(unsigned int uNode, unsigned int hToNode)
			{
				TProperty hProperties[2];

				hProperties[0] = GetActionProperty(uNode, uNode == 0);
				hProperties[1] = GetActionProperty(hToNode, false);

				return (*m_pHandler)[&hProperties[0]]->CalculateH(&hProperties[1]);
			}

			///<summary>
			/// Used to find identical nodes
			///</summary>
			bool Equals(unsigned int uNode, unsigned int uValue) 
			{
				TProperty hProperties[2];

				hProperties[0] = GetActionProperty(uNode, uNode == 0);
				hProperties[1] = GetActionProperty(uValue, false);

				return (*m_pHandler)[&hProperties[0]]->Equals(&hProperties[1]);
			}

			///<summary>
			/// Checks if last state matches with the goal state
			///</summary>
			bool Match(unsigned int uGoal) 
			{
				TProperty hState = m_hStates.back();

				return (*m_pHandler)[&hState]->Match(GetActionFromNode(uGoal)->GetCondition(false));
			}

			///<summary>
			/// Preparation before planning
			///</summary>
			void PerpareGathering() 
			{
				count = 0;

				m_hStates.clear();

				UpdateStates();
			}


			///<summary>
			/// Gathers list of available nodes (actions) that satisfy goal state based on last state 
			///</summary>
			void GatherData(unsigned int uGoal) 
			{
				Reset();

				bool bNeeds;
				
				Action::XBase<TProperty> *pAction;

				TProperty hState = m_hStates.back();

				for (int cost, i = 0, c = m_pActions->GetSize(), stateIdx = m_hStates.size() - 1; i < c; i++)
				{
					pAction = GetAction(i);

					m_hActionCost[m_bUseBalancer](bNeeds, cost, i, pAction);

					AddNodeIfNeeded(&hState, pAction, i, cost, stateIdx, bNeeds);
				}
			}


			//							//
			// Planning core methods	//
			//							//


			///<summary>
			/// Main planning method that copies provided action with hProperties (precondition - current state, postcondition - goal state) and begin searching solution
			///</summary>
			int MakePlan(Action::IBase *pGoal) 
			{

				m_hStates.clear();

				m_pAction->Copy(pGoal);

				m_pPlanner->Search(this, 0, 0, &m_hResult);

				return m_hResult.cost;
			}

			int GetPlanLength()
			{
				return m_hResult.count;
			}

			Action::IBase* GetPlannedActionAt(int iIdx)
			{
				return GetActionFromNode(m_hResult.nodes[iIdx]);
			}

			void EnableBalancer()
			{
				m_bUseBalancer = m_pBalance != NULL;
			}

			void DisableBalancer()
			{
				m_bUseBalancer = false;
			}

			void SetDefaultProceduralCostStrategy()
			{
				m_hProceduralCostStrategy = TProceduralCostStrategyDelegate::FromFunction<XBase::DefautProceduralCostStrategy>();
			}

			void SetProceduralCostStrategy(TProceduralCostStrategyDelegate &hProceduralCostStrategy)
			{
				m_hProceduralCostStrategy = hProceduralCostStrategy;
			}

		protected:

			//						//
			// Node cipher methods	//
			//						//

			unsigned int EncodeNode(int iState, int iIndex)
			{
				return iState << m_uActionsMask | ((iIndex + 1) & ((0x1u << m_uActionsMask) - 1));
			}

			int DecodeNode(unsigned int uNode)
			{
				return (uNode - 1) & ((0x1u << m_uActionsMask) - 1);
			}

			//															//
			// Action &| Property getter methods with encoded uNode...	//
			//															//


			///<summary>
			/// Get action from encoded uNode, if uNode is invalid will return current action or null (If allowed)
			///</summary>
			Action::XBase<TProperty> *GetActionFromNode(unsigned int uNode, bool bAllowNull = false) 
			{
				int iIndex = DecodeNode(uNode);

				return uNode > 0 && iIndex >= 0 ? (Action::XBase<TProperty>*)((*m_pActions)(m_pManager, iIndex)) : bAllowNull ? NULL : m_pAction;
			}

			///<summary>
			/// Get action property with uNode & condition eFlags
			///</summary>
			TProperty GetActionProperty(unsigned int uNode, Action::IBase::EConditionFlags eFlags, bool bSecond)
			{
				return GetActionProperty(uNode, Action::IBase::GetConditionFromFlags(eFlags, bSecond));
			}

			///<summary>
			/// Get action property from encoded uNode
			///</summary>
			TProperty GetActionProperty(unsigned int uNode, bool bPreCondition, const char *szName = NULL) 
			{
				TProperty state = m_hStates.at(uNode >> m_uActionsMask);
				Action::XBase<TProperty> *action = GetActionFromNode(uNode, true);

				if (action == NULL)
				{
					return bPreCondition ? state : *m_pAction->GetCondition(false);
				}
				else
				{
					(*m_pHandler)[&state]->Apply(action->GetCondition(bPreCondition), szName);
				}

				return state;
			}

			//					//
			// Internal methods	//
			//					//

			///<summary>
			///If there states are empty apply action precondition as current state
			///</summary>
			void UpdateStates()
			{
				if (m_hStates.size() == 0)
				{
					m_hStates.push_back(*m_pAction->GetCondition(true));
				}
			}

			Action::XBase<TProperty> *GetAction(int iIndex)
			{
				return (Action::XBase<TProperty>*)(*m_pActions)(m_pManager, iIndex);
			}

			void AddNodeIfNeeded(TProperty *pState, Action::XBase<TProperty> *pAction, int iIndex, int &iCost, int &iStateIdx, bool bNeeds = true)
			{
				int iProceduralCost;

				if (iCost > 0 && bNeeds &&
					(*m_pHandler)[pAction->GetCondition(true)]->Match(pState, true) &&
					(iProceduralCost = pAction->GetProceduralCost() > 0))
				{
					m_hProceduralCostStrategy(iCost, iProceduralCost);

					count++;
					costs.push_back(iCost);
					nodes.push_back(EncodeNode(iStateIdx, iIndex));
				}
			}

			void NormalActionCost(bool& bNeeds, int &iCost, int iIndex, Action::XBase<TProperty> *pAction)
			{
				bNeeds = true;
				
				iCost = pAction->GetCost();
			}


			void BalancedActionCost(bool& bNeeds, int &iCost, int iIndex, Action::XBase<TProperty> *pAction)
			{
				bNeeds = pAction->GetCost() > 0;

				iCost = (int)(*m_pBalance)[iIndex].cost;
			}

			static void DefautProceduralCostStrategy(int& iCost, int& iProceduralCost) { }

			//
			// Component pointers
			// 
			Balance::Action::XSet *			m_pBalance; // Balanced action cost's component

			Action::XSet *					m_pActions; // Working set of action ids
			Action::XManager *				m_pManager; // Used to gather action instances with provided id
			
			Property::XHandler<TProperty> *	m_pHandler; // Used to access/modify action property data

			Utils::Planner::IBase *			m_pPlanner; // Plan solver with algorithm (eg:. AStar)
			Utils::Planner::IResult			m_hResult; // TODO: make result as component


			//
			// Internal var's
			//

			bool							m_bUseBalancer;
			TActionCostDelegate				m_hActionCost[2]; // Used for normal and balanced action cost getting;
			TProceduralCostStrategyDelegate m_hProceduralCostStrategy;

			Action::XBase<TProperty> *		m_pAction; // Copy of provided goal action

			unsigned int					m_uActionID; // Working action id
			unsigned int					m_uActionsMask; // Used as cipher 'key' for uNode, represents action set size bits count

			std::vector<TProperty>			m_hStates; // Essential for planning, represents current state for every provided currently active uNode
		};
	}
}

#endif