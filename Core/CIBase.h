#ifndef _GOAP_IBASE_H_
#define _GOAP_IBASE_H_

#include <utils-cpp/IPlannerBase.h>
#include <utils-cpp/IComponentBase.h>

#include "../Property/PXSet.h"
#include "../Action/AXManager.h"

#include "CXBlackBoard.h"

namespace GOAP
{
	namespace Core
	{

		struct IBase : public Utils::Planner::IData, public Utils::Component::IBase
		{

			virtual int MakePlan(Action::IBase *pGoal) = 0;

			virtual int GetPlanLength() = 0;

			virtual Action::IBase* GetPlannedActionAt(int iIdx) = 0;

			virtual void Reset() = 0;
			
			int MakePlan(unsigned int uActionID)
			{
				return MakePlan(BlackBoard.Get<Action::XManager>()->GetByID(uActionID));
			}

			int MakePlan(const char *szActionName)
			{
				return MakePlan(BlackBoard.Get<Action::XManager>()->GetByName(szActionName));
			}
		};
	}	
}

#endif