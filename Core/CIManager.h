#ifndef _GOAP_IMANAGER_H_
#define _GOAP_IMANAGER_H_

#include <utils-cpp/XComponentNull.h>
namespace GOAP
{
	namespace Core
	{
		typedef Utils::Component::XNull IManager;
	}
}

#endif
