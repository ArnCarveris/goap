#ifndef _GOAP_XBLACK_BOARD_H_
#define _GOAP_XBLACK_BOARD_H_

#include <typeinfo>

#include <utils-cpp/XComponentBase.h>

namespace GOAP
{
	namespace Core
	{
		struct XBlackBoard : public Utils::Component::XBase
		{
			void Initialize();

			void Update();

			void Shutdown();

		}; extern XBlackBoard BlackBoard;
	}
}

#endif