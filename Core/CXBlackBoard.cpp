#include "CXBlackBoard.h"

#include "../Action/AXManager.h"
#include "../Property/PXManager.h"
#include "../Decision/DXManager.h"

namespace GOAP
{
	namespace Core
	{
		XBlackBoard BlackBoard;

		void XBlackBoard::Initialize()
		{
			Create<Action::XManager>();
			Create<Property::XManager>();
			Create<Decision::XManager>();

			Utils::Component::XBase::Initialize(NULL);
		}

		void XBlackBoard::Update()
		{
			Utils::Component::XBase::Update(NULL);
		}

		void XBlackBoard::Shutdown()
		{
			Utils::Component::XBase::Shutdown(NULL);
		}

	}
}