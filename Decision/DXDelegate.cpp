#include "DXDelegate.h"

namespace GOAP
{
	namespace Decision
	{
		void XDelegate::SetDelegate(const TDelegate &hDelegate)
		{
			m_hDelegate = hDelegate;
		}

		float XDelegate::CalculateUtility()
		{
			return m_hDelegate(this);
		}
	}
}