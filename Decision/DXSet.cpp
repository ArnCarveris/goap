#include "DXSet.h"
#include "DINode.h"
#include "DXManager.h"

namespace GOAP
{
	namespace Decision
	{
		INode* XSet::operator ()(XManager *pManager, int iIndex)
		{
			return pManager->GetFromSetAtIndex(this, iIndex);
		}
	}
}