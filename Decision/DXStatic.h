#ifndef _GOAP_DECISION_XSTATIC_H_
#define _GOAP_DECISION_XSTATIC_H_

#include "DINode.h"

namespace GOAP
{
	namespace Decision
	{
		class XStatic : public INode
		{
		public:
			
			XStatic();

			void SetUtility(float fValue);

			float CalculateUtility();

		protected:

			float m_fUtility;

		};
	}
}

#endif