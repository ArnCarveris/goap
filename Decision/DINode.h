#ifndef _GOAP_DECISION_INODE_H_
#define _GOAP_DECISION_INODE_H_

#include <utils-cpp/XItem.h>
#include <utils-cpp/XSet.h>
#include <utils-cpp/XComponentNull.h>

#include "../Core/CXBlackBoard.h"

#include "DXSet.h"

namespace GOAP
{
	namespace Decision
	{
		class INode : public Utils::XItem
		{
		public:

			virtual float CalculateUtility() = 0;

		};

		T(Node) TNode *Create(const char *szName = NULL, XSet *pSet = NULL)
		{
			INode *pNode = new TNode();

			if (szName != NULL)
			{
				pNode->SetName(szName);
			}

			if (pSet != NULL)
			{
				Core::BlackBoard.Get < XManager >()->Register(pNode);

				pSet->Add(pNode->GetID());
			}

			return (TNode*)pNode;
		}

		T(Node) TNode *Create(Utils::Component::XBase *entity, const char *szName = NULL)
		{
			return Create<TNode>(szName, entity->Require<XSet>());
		}

	}
}

#endif