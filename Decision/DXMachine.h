#ifndef _GOAP_DECISION_MACHINE_H_
#define _GOAP_DECISION_MACHINE_H_

#include <utils-cpp/XSet.h>
#include <utils-cpp/Base.h>
#include <utils-cpp/IComponentBase.h>
#include <utils-cpp/XDelegate.h>

#include "../State/SXMachine.h"
#include "../Core/CIBase.h"
#include "../Action/AXSet.h"

namespace GOAP
{
	namespace Balance
	{
		namespace Decision
		{
			class XSet;
		}
	}

	namespace Decision
	{
		class INode;
		class XSet;
		class XManager;
		class XMachine : public Utils::Component::IBase
		{
		public:

			struct IStrategyData
			{
				float fMaxUtility;
				INode *pUtilityNode;
				std::vector<INode*> hEqualUtilities;

			};

			typedef Utils::Delegate::X4<void, INode*, float, float&, IStrategyData&> TStrategyDelegate;

			// Construction/Deconstruction //
			XMachine();
			~XMachine();
			
			// Component Interface methods //
			void Initialize(Utils::Component::XBase *entity);
			void Update(Utils::Component::XBase *entity);
			void Shutdown(Utils::Component::XBase *entity);

			void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args);
			// Decision making method //
			void Update(TStrategyDelegate &hStrategy, float fEpsilon);
			void Update(float fEpsilon);

			// Some accessors
			bool operator() (const char *name, bool value);

			void EnableBalancer();
			
			void DisableBalancer();

			void UnsetRange();

			void SetDefaultRange();

			void SetRange(float fMinValue, float fMaxValue);

			void SetRangeMin(float fValue);
			
			void SetRangeMax(float fValue);

			float GetRangeMin();

			float GetRangeMax();


		protected:
			
			typedef Utils::Delegate::X2<float, int, INode*> TUtilityValueDelegate;

			bool LimitUtilityValue(float &fValue);


			void Clear(IStrategyData &hStrategy);

			bool PopEqualUtility(IStrategyData &hStrategy);

			static void DefaultStrategy(INode *pNode, float fEpsilon, float &fUtilityValue, IStrategyData &hStrategy);
			
			float GetNormalUtilityValue(int iIndex, INode *pNode);

			float GetBalancedUtilityValue(int iIndex, INode *pNode);

			float						m_fRange[2]; // Range values min and max, using for limiting utility value

			bool						m_bUseBalancer;
			TUtilityValueDelegate		m_hUtilityValue[2];
			
			XManager *					m_pManager;
			XSet *						m_pNodes;
			Balance::Decision::XSet *	m_pBalance; // < Balanced utlity values
			State::XMachine *			m_pGosm; // < Goal orientied state machine

		};
	}
}


#endif