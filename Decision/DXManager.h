#ifndef _GOAP_DECISION_XMANAGER_H_
#define _GOAP_DECISION_XMANAGER_H_

#include <utils-cpp/XManager.h>

#include "../Core/CIManager.h"

#include "DINode.h"

namespace GOAP
{
	namespace Decision
	{
		class XManager : public Utils::XManager<INode*, unsigned int>, public Core::IManager
		{
		protected:

			INode *GetByItemSet(unsigned int iID)
			{
				return iID > 0 ? *items.GetAtIndex(iID - 1) : NULL;
			}
		};
	}
}

#endif