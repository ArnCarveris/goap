#include "DXStatic.h"

namespace GOAP
{
	namespace Decision
	{
		XStatic::XStatic() : m_fUtility(0.0f) { }

		void XStatic::SetUtility(float fValue)
		{
			m_fUtility = fValue;
		}

		float XStatic::CalculateUtility()
		{
			return m_fUtility;
		}
	}
}