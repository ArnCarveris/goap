#ifndef _GOAP_DECISION_XDELEGATE_H_
#define _GOAP_DECISION_XDELEGATE_H_

#include "DINode.h"

#include <utils-cpp/XDelegate.h>

namespace GOAP
{
	namespace Decision
	{
		class XDelegate : public INode
		{
		public:

			typedef Utils::Delegate::X1<float, INode*> TDelegate;

			void SetDelegate(const TDelegate & hDelegate);

			float CalculateUtility();

		protected:

			TDelegate m_hDelegate;
		};
	}
}

#endif