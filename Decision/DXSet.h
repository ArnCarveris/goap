#ifndef _GOAP_DECISION_XSET_H_
#define _GOAP_DECISION_XSET_H_

#include <utils-cpp/XSet.h>
#include <utils-cpp/XComponentNull.h>

namespace GOAP
{
	namespace Decision
	{
		class INode;
		class XManager;
		class XSet : public Utils::XIDSet, public Utils::Component::XNull
		{

		public:

			INode* operator ()(XManager *pManager, int iIndex);
		};
	}
}

#endif