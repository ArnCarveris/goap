#include "DXMachine.h"

#include <utils-cpp/XComponentBase.h>

#include <math.h>
#include <float.h>

#include "../Core/CXBlackBoard.h"

#include "../Balance/Decision/BDXSet.h"
#include "../Balance/Decision/BDXNode.h"

#include "DINode.h"
#include "DXManager.h"

namespace GOAP
{
	namespace Decision
	{

		XMachine::XMachine() : m_pBalance(NULL), m_pGosm(NULL), m_pNodes(NULL)
		{
			m_pManager = Core::BlackBoard.Get<XManager>();

			m_hUtilityValue[0] = TUtilityValueDelegate::FromMethod < XMachine, &XMachine::GetNormalUtilityValue >(this);
			m_hUtilityValue[1] = TUtilityValueDelegate::FromMethod < XMachine, &XMachine::GetBalancedUtilityValue >(this);

			DisableBalancer();

			UnsetRange();
		}

		XMachine::~XMachine()
		{
			m_pGosm = NULL;
			m_pNodes = NULL;
			m_pManager = NULL;
			m_pBalance = NULL;

			DisableBalancer();
		}

		void XMachine::Initialize(Utils::Component::XBase *entity)
		{
			entity->RegisterMethod<XMachine, &XMachine::OnInitialize >("OnInitialize", this);
		}

		void XMachine::OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
		{
			m_pGosm = entity->Require<State::XMachine>();
		}

		void XMachine::Update(Utils::Component::XBase *entity)
		{
			m_pNodes = entity->Require <XSet>();
			m_pBalance = entity->Require < Balance::Decision::XSet>();

			EnableBalancer();
		}

		void XMachine::Shutdown(Utils::Component::XBase *entity)
		{
			m_pGosm = NULL;
			m_pBalance = NULL;

			DisableBalancer();
		}

		void XMachine::Update(float fEpsilon)
		{
			static TStrategyDelegate hStrategy = TStrategyDelegate::FromFunction<&XMachine::DefaultStrategy>();

			Update(hStrategy, fEpsilon);
		}

		void XMachine::Update(TStrategyDelegate &hStrategy, float fEpsilon)
		{
			if (m_pNodes == NULL || m_pNodes->GetSize() < 1)
			{
				return;
			}

			float fUtilityValue;
			IStrategyData hStrategyData[2];

			INode *pNode;

			for (size_t i = 0; i < 2; i++)
			{
				Clear(hStrategyData[i]);
			}

			for (int i = 0, c = m_pNodes->GetSize(); i < c; i++)
			{
				pNode = (*m_pNodes)(m_pManager, i);
				
				fUtilityValue = m_hUtilityValue[m_bUseBalancer](i, pNode);

				if (LimitUtilityValue(fUtilityValue))
				{
					hStrategy(pNode, fEpsilon, fUtilityValue, hStrategyData[fUtilityValue > 0.0f]);
				}
			}

			for (size_t i = 0; i < 2; i++)
			{
				if (hStrategyData[i].pUtilityNode == NULL)
				{
					continue;
				}

				const bool bValue = i > 0;

				do
				{
					(*m_pGosm)(State::XMachine::EState::Goal, hStrategyData[i].pUtilityNode->GetName(), bValue, true);
					
				} while (PopEqualUtility(hStrategyData[i]));
			}

			m_pGosm->MakePlan();
		}

		bool XMachine::operator() (const char *name, bool value)
		{
			return (*m_pGosm)(State::XMachine::EState::Current, name, value);
		}

		void XMachine::EnableBalancer()
		{
			m_bUseBalancer = m_pBalance != NULL;
		}

		void XMachine::DisableBalancer()
		{
			m_bUseBalancer = false;
		}

		void XMachine::UnsetRange()
		{
			SetRange(-FLT_MAX, FLT_MAX);
		}

		void XMachine::SetDefaultRange()
		{
			SetRange(-1.0f, 1.0f);
		}

		void XMachine::SetRange(float fMinValue, float fMaxValue)
		{
			SetRangeMin(fMinValue);
			SetRangeMax(fMaxValue);
		}

		void XMachine::SetRangeMin(float fValue)
		{
			m_fRange[0] = fValue > 0.0f ? -fValue : fValue;
		}
		void XMachine::SetRangeMax(float fValue)
		{
			m_fRange[1] = fValue < 0.0f ? -fValue : fValue;
		}

		float XMachine::GetRangeMin()
		{
			return m_fRange[0];
		}

		float XMachine::GetRangeMax()
		{
			return m_fRange[1];
		}

		bool XMachine::LimitUtilityValue(float &fValue)
		{
			if (fValue < m_fRange[0])
			{
				fValue = m_fRange[0];
			}
			else if (fValue > m_fRange[1])
			{
				fValue = m_fRange[1];
			}

			return fValue != 0.0f;
		}

		void XMachine::Clear(IStrategyData &hStrategy)
		{
			hStrategy.pUtilityNode = NULL;

			hStrategy.fMaxUtility = 0.0f;

			hStrategy.hEqualUtilities.clear();
		}

		bool XMachine::PopEqualUtility(IStrategyData &hStrategy)
		{
			if (hStrategy.hEqualUtilities.size() > 0)
			{
				hStrategy.pUtilityNode = hStrategy.hEqualUtilities.back();

				hStrategy.hEqualUtilities.pop_back();
			}
			else
			{
				hStrategy.pUtilityNode = NULL;
			}

			return hStrategy.pUtilityNode != NULL;
		}

		float XMachine::GetNormalUtilityValue(int iIndex, INode *pNode)
		{
			return pNode->CalculateUtility();
		}

		float XMachine::GetBalancedUtilityValue(int iIndex, INode *pNode)
		{
			return  (*m_pBalance)[iIndex].GetValue();
		}

		void XMachine::DefaultStrategy(INode *pNode, float fEpsilon, float &fUtilityValue, IStrategyData &hStrategy)
		{
			fUtilityValue = fabs(fUtilityValue);

			if (hStrategy.fMaxUtility < fUtilityValue)
			{
				hStrategy.fMaxUtility = fUtilityValue;
				hStrategy.pUtilityNode = pNode;
			}
			else if (fabs(hStrategy.fMaxUtility - fUtilityValue) < fEpsilon)
			{
				hStrategy.hEqualUtilities.push_back(pNode);
			}
		}
	}
}
