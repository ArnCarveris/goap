#ifndef _GOAP_STATE_VALIDATOR_H_
#define _GOAP_STATE_VALIDATOR_H_

#include <utils-cpp/Base.h>
#include <utils-cpp/IMaskValidator.h>
#include <utils-cpp/XComponentNull.h>

#include "SIValidator.h"


namespace GOAP
{
	namespace State
	{
		T(Mask) class XValidator : public Utils::Component::XNull, public Utils::IMaskValidator <TMask, IValidator::EAction, XValidator<TMask> >
		{
		public:

			XValidator() : Utils::IMaskValidator <TMask, IValidator::EAction, XValidator<TMask> >(6, IValidator::EAction::None, 0, this)
			{
				Initialize();
			}

			XValidator(TMask tDefautMask) : Utils::IMaskValidator <TMask, IValidator::EAction, XValidator<TMask> >(6, IValidator::EAction::None, tDefautMask, this)
			{
				Initialize();
			}


			IValidator::EAction ValidateFromStage(IValidator::EStage eStage)
			{
				return Validate(GetStageMask(eStage));
			}

			bool SetStageMask(TMask tMask, IValidator::EStage eStage)
			{
				return SetMask(tMask, (size_t)eStage);
			}

			TMask GetStageMask(IValidator::EStage eStage)
			{
				return GetMask((size_t)eStage);
			}

			void ClearStageMask(IValidator::EStage eStage)
			{
				SetMask(defaultMask, eStage);
			}

			IValidator::EStage GetStage()
			{
				return (IValidator::EStage)eStage;
			}


		protected:

#define RegisterX(_name_) RegisterStage(&XValidator::Validate##_name_)
			void Initialize()
			{	
				RegisterX(Changes);
				RegisterX(Plan);
				RegisterX(Goal);
				RegisterX(Performed);
				RegisterX(PreCondition);
				RegisterX(PostCondition);
			}
#undef RegisterX

			IValidator::EAction ValidateChanges(TMask tMask, EStatus eStatus)
			{
				return tMask == defaultMask ? IValidator::EAction::Break : IValidator::EAction::Continue;
			}

			IValidator::EAction ValidatePlan(TMask tMask, EStatus eStatus)
			{
				return eStatus == EStatus::Failed ? IValidator::EAction::Break : IValidator::EAction::Continue;
			}

			IValidator::EAction ValidateGoal(TMask tMask, EStatus eStatus)
			{
				switch (eStatus)
				{
				case EStatus::Success: return IValidator::EAction::Reset;
				case EStatus::Partial: return IValidator::EAction::Replan;
				}

				return IValidator::EAction::Continue;
			}

			IValidator::EAction ValidatePerformed(TMask tMask, EStatus eStatus)
			{
				return eStatus == EStatus::Failed || eStatus == EStatus::Empty ? IValidator::EAction::Continue : IValidator::EAction::Replan;
			}

			IValidator::EAction ValidatePreCondition(TMask tMask, EStatus eStatus)
			{
				return eStatus == EStatus::Failed || eStatus == EStatus::Empty ? IValidator::EAction::Continue : IValidator::EAction::Backward;
			}

			IValidator::EAction ValidatePostCondition(TMask tMask, EStatus eStatus)
			{
				return eStatus == EStatus::Success ? IValidator::EAction::Forward : IValidator::EAction::Break;
			}
		};
	}
}

#endif