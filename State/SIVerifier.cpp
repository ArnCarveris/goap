#include "SIVerifier.h"

namespace GOAP
{
	namespace State
	{
		bool IVerifier::Validate(IValidator::EAction &eAction)
		{
			Clear();

			bool bClear = false;

			do
			{
				switch ((eAction = ValidateFromStage(IValidator::EStage::Changes)))
				{
				case IValidator::EAction::None:
				case IValidator::EAction::Validation:
				case IValidator::EAction::Break: return false;
				case IValidator::EAction::Continue: break;

				default:

					bClear = true; break;
				}

			} while (bClear == false);

			ClearStageMask(IValidator::EStage::Changes);

			return true;
		}

		void IVerifier::ClearValidationStages(int iLevel)
		{

#define Level(_level_) _level_ : if (iLevel < _level_) return 

			switch (iLevel)
			{
				case Level(1);

					ClearStageMask(IValidator::EStage::PreCondition);
					ClearStageMask(IValidator::EStage::PostCondition);

				case Level(2);

					ClearStageMask(IValidator::EStage::Changes);
					ClearStageMask(IValidator::EStage::Performed);

				case Level(3);

					ClearStageMask(IValidator::EStage::Goal);

				case Level(4);

					ClearStageMask(IValidator::EStage::Plan);

					break;
			}

#undef Level
		}

		void IVerifier::GatherPlan(Core::IBase *pGoap)
		{
			m_pPlan->Clear();

			for (int i = 0, l = pGoap->GetPlanLength(); i < l; i++)
			{
				m_pPlan->Add(pGoap->GetPlannedActionAt(i));
			}
		}


		void IVerifier::InitializeProperty(Utils::Component::XBase *entity)
		{
			m_pHandler = entity->Require<Property::IHandler>();
			m_pContext = entity->Require<Property::XSet>();
		}
	}
}