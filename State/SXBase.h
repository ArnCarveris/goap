#ifndef _GOAP_STATE_XBASE_H_
#define _GOAP_STATE_XBASE_H_

#include <utils-cpp/XStateManager.h>
#include <utils-cpp/IStateBase.h>
#include <utils-cpp/XStateSet.h>

#include "../Action/AIBase.h"

namespace GOAP
{
	namespace State
	{
		class XMachine;
		class XBase : public Utils::State::IBase
		{
		public:

			void Bind(const char *szActionName = NULL);
			void Bind(unsigned int uActionID);
			void Bind(Action::IBase *pAction);

			void Reset(const char *szName);

			void RebindOwner();

			void SetOwner(Utils::Component::XBase *entity);

			Action::IBase *GetAction();

			T(Action) void CreateAction(int iCost = 1)
			{
				Bind(Action::Create<TAction>(m_pEntity, szName, iCost));
			}

		protected:
			void InitValue(bool bPreCondition, const char * szName, bool bValue);
			void ApplyValue(const char *szName);


			XMachine *					m_pMachine;
			Utils::Component::XBase *	m_pEntity;

			Action::IBase *				m_pAction;
			Property::IHandler *		m_pHandler;
		};

		T(State) TState* Create(const char *szName, Utils::State::XSet *pSet = NULL)
		{
			XBase *pState = new TState();

			pState->Reset(szName);

			if (pSet != NULL)
			{
				Utils::State::XManager::GetInstance()->Register(pState);

				pSet->Add(pState->GetID());
			}

			return (TState*)pState;
		}

		T(State) TState* Create(Utils::Component::XBase *entity, const char *szName)
		{
			TState *pState = Create<TState>(szName, entity->Require<Utils::State::XSet>());

			pState->SetOwner(entity);

			return pState;
		}
	}
}

#endif