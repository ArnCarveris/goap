#ifndef _GOAP_STATE_MACHINE_H_
#define _GOAP_STATE_MACHINE_H_

#include <utils-cpp/Base.h>
#include <utils-cpp/XStateMachine.h>
#include <utils-cpp/XDictionary.h>
#include <utils-cpp/XSet.h>
#include <utils-cpp/IStateBase.h>

#include "../Action/AIBase.h"
#include "../Core/CIBase.h"

#include "SXBase.h"
#include "SIVerifier.h"

namespace GOAP
{

	namespace State
	{

		class XMachine : public Utils::State::XMachine
		{
		public: // Types

			enum EStatus
			{
				Idle = 0,
				Planning = 1,
				Performing = 2,
				Done = 3
			};

			enum EState
			{
				Current = 0,
				Goal = 1,
				None = 2
			};


		public: 

			// Construction/Deconstruction //
			XMachine();
			virtual ~XMachine();

			// Component interface methods //
			void Initialize(Utils::Component::XBase *entity);
			void Shutdown(Utils::Component::XBase *entity);

			// Component event methods
			void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args);
			
			// Core methods
			int MakePlan();
			void Validate();
			void GoToNextState();

			// State accessors via property handler
			Property::IHandler *operator() (EState eState);
			bool operator() (EState eState, const char *szName);
			bool operator() (EState eState, const char *szName, bool bValue, bool bNotify = false);


			// Action property atom appling to state
			bool DoAction(EState eState, const char *szName, Action::IBase *pAction);

			// Various accessors
			EStatus GetStatus();
			static const char* GetStatusName(EStatus eStatus);

		protected: 
			
			//
			// Internal Methods
			//
			
			void Initialize();

			bool Match(EState eState, XBase *pState, bool bPreCondition);

			void StateValueDidChange(EState eState, const char *szName, bool bValue);

			// State changing methods //
			void GoToPreviousState();
			
			void GoToDefaultState();
			
			void GoToPlanningState();
			
			void GoToState(XBase *pState);

			// State changing allowing //
			bool CanExit();
			
			bool CanEnter(Utils::State::IBase *pState);

			// Plan state methods //
			void UpdatePlannedState(bool bForce = false);

			void SetPlannedState(int iStateIdx);

			XBase *GetPlannedState(int iStateIdx);

			//
			// Properties
			//

			typedef Utils::Delegate::X0<void> TValidationDelegate;
			
			IVerifier *				m_pVerifier;

			TValidationDelegate		m_hValidationActions[4];


			EStatus					m_eStatus;

			XBase *					m_pNext;

			Action::IBase *			m_pState;

			int						m_iPlanState;

			Core::IBase	*			m_pGoap;

			Property::IHandler *	m_pHandler;

		};
	}
}


#endif