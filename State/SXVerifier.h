#ifndef _GOAP_STATE_XVERIFIER_H_
#define _GOAP_STATE_XVERIFIER_H_

#include <utils-cpp/Base.h>
#include <utils-cpp/XComponentBase.h>

#include "SIVerifier.h"
#include "SXValidator.h"

namespace GOAP
{
	namespace State
	{
		T(Mask) class XVerifier : public IVerifier
		{
		public:

			void Initialize(Utils::Component::XBase *entity)
			{
				m_pManager = Core::BlackBoard.Get < Action::XManager > ();

				m_pPlan = new Action::XSet();

				entity->RegisterMethod<XVerifier, &XVerifier::OnInitialize >("OnInitialize", this);
			}

			void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
			{
				InitializeProperty(entity);

				m_pValidator = entity->Require < XValidator < TMask > >();
			}

			void Update(Utils::Component::XBase *entity) { }

			void Shutdown(Utils::Component::XBase *entity)
			{
				m_pHandler = NULL;
				m_pValidator = NULL;
				m_pManager = NULL;

				delete m_pPlan;

				m_pPlan = NULL;
			}

			bool MaskValidationStage(IValidator::EStage eStage, bool bClear)
			{
				const int iCount = m_pPlan->GetSize();

				if (m_pPlan == NULL || iCount < 1)
				{
					return false;
				}

				TMask tMask = bClear ? (TMask)(0) : m_pValidator->GetStageMask(eStage);

				for (int i = 0; i < iCount; ++i)
				{
					Action::IBase *pAction = (*m_pPlan)(m_pManager, i);

					(*m_pHandler)(pAction, true)->IncludeMask(&tMask);
					(*m_pHandler)(pAction, false)->IncludeMask(&tMask);
				}

				return m_pValidator->SetStageMask(tMask, eStage);
			}

			bool MaskValidationStage(const char *szName, IValidator::EStage eStage)
			{
				return MaskValidationStage((TMask)(1) << m_pContext->GetAtomIndex(szName), eStage);
			}

			XBase *GetValidState(int &iStep)
			{
				Action::IBase *pState = NULL;

				TMask tMask[2] =														// Validator Stages tMask info
				{
					(TMask)(0),															//0) State pAction tMask
					m_pValidator->GetStageMask(IValidator::EStage::PreCondition) &
					m_pValidator->GetStageMask(IValidator::EStage::Changes)				//1) Changed current pAction precondition tMask
				};

				m_tMask = m_pValidator->GetStageMask(IValidator::EStage::Performed);

				Action::IBase *pAction = NULL;

				iStep--;

				while (pState == NULL && --iStep >= 0) // Go backward from current pAction in plan to find valid pAction that resolves changes in performed eStage tMask
				{
					pState = (*m_pPlan)(m_pManager, iStep);
					pAction = pState;

					tMask[0] = (TMask)(0);

					(*m_pHandler)(pAction, false)->IncludeMask(&tMask[0]); // Get pAction postcondition tMask

					if ((tMask[0] & tMask[1]) == (TMask)(0)) // Checking if pAction postcondition is in changed set of current pAction precondition 
					{
						pState = NULL;
					}

					(*m_pHandler)(pAction, true)->IncludeMask(&tMask[0]); // Include pAction precondition tMask

					m_tMask = ~(m_tMask & tMask[0]); // Discard pAction tMask from performed eStage tMask
				}

				return NULL;
			}

			void SetVaidState()
			{
				m_pValidator->SetStageMask(m_tMask, IValidator::EStage::Performed);
			}

			void ApplyPlannedState(int iStep)
			{
				Action::IBase *pAction = (*m_pPlan)(m_pManager, iStep);

				MaskValidationStage(m_pValidator->GetStageMask(IValidator::EStage::PreCondition) | m_pValidator->GetStageMask(IValidator::EStage::PostCondition), IValidator::EStage::Performed);

				if (pAction == NULL)
				{
					ClearValidationStages(1);
				}
				else
				{
					MaskValidationStage(pAction, true, IValidator::EStage::PreCondition, true);
					MaskValidationStage(pAction, false, IValidator::EStage::PostCondition, true);
				}
			}

			void Clear()
			{
				m_pValidator->Clear();
			}

		protected:

			bool MaskValidationStage(TMask tMask, IValidator::EStage eStage)
			{
				return m_pValidator->SetStageMask(m_pValidator->GetStageMask(eStage) | tMask, eStage);
			}

			IValidator::EAction ValidateFromStage(IValidator::EStage eStage)
			{
				return m_pValidator->ValidateFromStage(eStage);
			}

			void ClearStageMask(IValidator::EStage eStage)
			{
				m_pValidator->ClearStageMask(eStage);
			}

			bool MaskValidationStage(Action::IBase *pAction, bool bPreCondition, IValidator::EStage eStage, bool bClear)
			{
				if (pAction == NULL)
				{
					return false;
				}

				m_tMask = bClear ? (TMask)(0) : m_pValidator->GetStageMask(eStage);

				(*m_pHandler)(pAction, bPreCondition)->IncludeMask(&m_tMask);

				return m_pValidator->SetStageMask(m_tMask, eStage);
			}

			bool MaskValidationStage(Action::IBase *pAction, IValidator::EStage eStage, bool bClear)
			{
				if (pAction == NULL)
				{
					return false;
				}

				m_tMask = bClear ? (TMask)(0) : m_pValidator->GetStageMask(eStage);

				(*m_pHandler)(pAction, true)->IncludeMask(&m_tMask);
				(*m_pHandler)(pAction, false)->IncludeMask(&m_tMask);

				return m_pValidator->SetStageMask(m_tMask, eStage);
			}

			TMask					m_tMask;

			XValidator<TMask> *		m_pValidator;

			Action::XManager *		m_pManager;
		};
	}
}

#endif