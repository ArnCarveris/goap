#ifndef _GOAP_STATE_IVERIFIER_H_
#define _GOAP_STATE_IVERIFIER_H_

#include <utils-cpp/IComponentBase.h>

#include "SIValidator.h"

#include "../Action/AIBase.h"
#include "../Action/AXSet.h"

#include "../Property/PIHandler.h"

#include "../Core/CIBase.h"

namespace GOAP
{
	namespace State
	{
		class XBase;
		class IVerifier : public IValidator, public Utils::Component::IBase
		{
		public:

			virtual bool MaskValidationStage(IValidator::EStage eStage, bool bClear) = 0;

			virtual bool MaskValidationStage(const char *szName, IValidator::EStage eStage) = 0;

			virtual XBase *GetValidState(int &iStep) = 0;

			virtual void SetVaidState() = 0;

			virtual void ApplyPlannedState(int iStep) = 0;

			virtual void Clear() = 0;

			bool Validate(IValidator::EAction &eAction);

			void ClearValidationStages(int iLevel);

			void GatherPlan(Core::IBase *pGoap);

		protected:

			virtual IValidator::EAction ValidateFromStage(IValidator::EStage eStage) = 0;

			virtual void ClearStageMask(IValidator::EStage eStage) = 0;

			virtual bool MaskValidationStage(Action::IBase *pAction, bool bPreCondition, IValidator::EStage eStage, bool bClear) = 0;

			virtual bool MaskValidationStage(Action::IBase *pAction, IValidator::EStage eStage, bool bClear) = 0;

			void InitializeProperty(Utils::Component::XBase *entity);

			Property::XSet *		m_pContext;
			Property::IHandler *	m_pHandler;
			Action::XSet *			m_pPlan;
		};
	}
}

#endif