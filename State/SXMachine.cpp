#include "SXMachine.h"

#include <utils-cpp/Base.h>

#include <utils-cpp/XComponentBase.h>

#include "../Property/PIHandler.h"

#include "SXValidator.h"
#include "SXBase.h"
#include "SXManager.h"


namespace GOAP
{
	namespace State
	{

		XMachine::XMachine() : Utils::State::XMachine()
		{
			Initialize();
		}

		XMachine::~XMachine()
		{
			m_pVerifier = NULL;
			
			m_pNext = NULL;

			m_pGoap = NULL;

			delete m_pState;

			m_pState = NULL;

			Utils::State::XMachine::~XMachine();
		}

		void XMachine::Initialize()
		{
			states = NULL;

			m_eStatus = EStatus::Idle;

			m_pHandler = NULL;
			m_pNext = NULL;
			m_pState = NULL;
			m_pVerifier = NULL;


#define Set(_action_, _callback_) m_hValidationActions[(int)(_action_ )] = TValidationDelegate::FromMethod < XMachine, & XMachine:: ## _callback_ > (this);

			Set(IValidator::EAction::Reset,		GoToDefaultState);
			Set(IValidator::EAction::Replan,	GoToPlanningState);
			Set(IValidator::EAction::Backward,	GoToPreviousState);
			Set(IValidator::EAction::Forward,	GoToNextState);

#undef Set

		}

		void XMachine::Initialize(Utils::Component::XBase *entity)
		{
			m_pState = NULL;
			
			entity->RegisterMethod<XMachine, &XMachine::OnInitialize >("OnInitialize", this);
		}

		void XMachine::OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
		{
			m_pGoap = entity->Require <Core::IBase>();
			m_pHandler = entity->Require <Property::IHandler>();
			m_pVerifier = entity->Require <IVerifier>();
		}

		void XMachine::Shutdown(Utils::Component::XBase *entity)
		{
			m_pGoap = NULL;
			m_pHandler = NULL;
			m_pVerifier = NULL;

			if (m_pState != NULL)
			{
				delete m_pState;

				m_pState = NULL;
			}
		}


		bool XMachine::operator() (EState eState, const char *szName)
		{
			return eState >= EState::Current && eState <= EState::Goal && (*m_pHandler)(this->m_pState, eState == EState::Current)->GetValue(szName);
		}

		bool XMachine::operator() (EState eState, const char *pName, bool bValue, bool bNotify)
		{
			bool bUnique = false;

			if (this->m_pState == NULL && m_pHandler != NULL)
			{
				this->m_pState = m_pHandler->CreateState(NULL);
			}

			if (eState >= EState::Current && eState <= EState::Goal && (bUnique = (*m_pHandler)(this->m_pState, eState == EState::Current)->SetValue(pName, bValue)) && bNotify)
			{
				StateValueDidChange(eState, pName, bValue);
			}

			return bUnique;
		}

		Property::IHandler *XMachine::operator() (EState eState)
		{
			return (*m_pHandler)(this->m_pState, eState == EState::Current);
		}

		bool XMachine::DoAction(EState eState, const char *szName, Action::IBase *pAction)
		{
			m_pHandler->Apply(Action::IBase::GetFlags(eState == EState::Current, false), this->m_pState, pAction, szName);

			StateValueDidChange(eState, szName, (*m_pHandler)(pAction, false)->GetValue(szName));

			return true;
		}

		void XMachine::StateValueDidChange(EState eState, const char *szName, bool bValue)
		{
			switch (eState)
			{

			case XMachine::Goal:

				m_pVerifier->MaskValidationStage(szName, IValidator::EStage::Goal);

				break;
			}

			m_pVerifier->MaskValidationStage(szName, IValidator::EStage::Changes);
			
			Validate();
		}


		void XMachine::Validate()
		{
			IValidator::EAction eAction;

			if (m_pVerifier->Validate(eAction))
			{
				m_hValidationActions[(int)eAction]();
			}
		}

		bool XMachine::Match(EState eState, XBase *pState, bool bPreCondition)
		{
			return pState == NULL || IsNullState(pState) || m_pHandler->Match(Action::IBase::GetFlags(eState == EState::Current, bPreCondition), this->m_pState, pState->GetAction());
		}

		bool XMachine::CanExit() //Post Condition Check
		{
			return Match(EState::Current, (XBase*)current, false);
		}

		bool XMachine::CanEnter(Utils::State::IBase *pState) //Pre Condition Check
		{
			if (Match(EState::Current, (XBase*)pState, true) == false)
			{
				return false;
			}

			m_pVerifier->ApplyPlannedState(m_iPlanState++);

			UpdatePlannedState(true);

			return true;
		}

		void XMachine::SetPlannedState(int iStateIdx)
		{
			m_pNext = GetPlannedState(iStateIdx);
		}

		XBase* XMachine::GetPlannedState(int iStateIdx)
		{
			return iStateIdx < 0 || iStateIdx >= m_pGoap->GetPlanLength() ? NULL : (XBase*)GetState(m_pGoap->GetPlannedActionAt(iStateIdx)->GetName());
		}

		void XMachine::UpdatePlannedState(bool bForce)
		{
			if (bForce || m_pNext == NULL)
			{
				SetPlannedState(m_iPlanState);
			}
		}

		void XMachine::GoToNextState()
		{
			UpdatePlannedState();

			GoToState(m_pNext);
		}

		void XMachine::GoToPreviousState()
		{

			XBase *pState = NULL;

			pState = m_pVerifier->GetValidState(m_iPlanState);

			if (pState != NULL) // Enter to m_pState that performs valid action if possible, overwise do replan
			{
			 	m_pVerifier->ClearValidationStages(1);

				if (DoEnter(pState))
				{
					m_pVerifier->SetVaidState();

					return;
				}
			}

			GoToPlanningState();
		}

		void XMachine::GoToDefaultState()
		{
			m_pVerifier->ClearValidationStages(3);

			m_pNext = NULL;

			GoToState(m_pNext);
		}

		void XMachine::GoToPlanningState()
		{
			MakePlan();
		}

		void XMachine::GoToState(XBase *pState)
		{
			ChangeState(pState);

			m_eStatus = IsNullState(current) ? m_iPlanState - 1 > 0 ? EStatus::Done : EStatus::Idle : EStatus::Performing;
		}

		int XMachine::MakePlan()
		{
			m_eStatus = EStatus::Planning;

			m_pNext = NULL;

			m_iPlanState = 0;

			m_pGoap->MakePlan(m_pState);
			
			m_pVerifier->ClearValidationStages(2);

			m_pVerifier->GatherPlan(m_pGoap); // TODO: replace this in future, plan is must be also component

			m_pVerifier->MaskValidationStage(IValidator::EStage::Plan, true);

			return 0;
		}

		XMachine::EStatus XMachine::GetStatus()
		{
			return m_eStatus;
		}

		const char *XMachine::GetStatusName(XMachine::EStatus eStatus)
		{
			static const char* names[4] = {
				"Idle",
				"Planning",
				"Performing",
				"Done",
			};

			return names[(int)eStatus];
		}
	}
}