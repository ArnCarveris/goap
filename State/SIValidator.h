#ifndef _GOAP_STATE_IVALIDATOR_H_
#define _GOAP_STATE_IVALIDATOR_H_

namespace GOAP
{
	namespace State
	{
		struct IValidator
		{
			enum EAction
			{
				Reset = 0,
				Replan = 1,
				Backward = 2,
				Forward = 3,

				Validation = 4,

				Break = 5,
				Continue = 6,
				None = 7
			};

			enum EStage
			{
				Changes = 0,
				Plan = 1,
				Goal = 2,
				Performed = 3,
				PreCondition = 4,
				PostCondition = 5
			};

			const char *GetStageName(EStage stage)
			{
				static const char* names[] = {
					"Changes",
					"Plan",
					"Goal",
					"Performed",
					"PreCondition",
					"PostCondition"
				};

				return names[(int)stage];
			}
		};
	}
}

#endif