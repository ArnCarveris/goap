#include "SXBase.h"
#include "SXMachine.h"
#include "../Core/CXBlackBoard.h"

namespace GOAP
{
	namespace State
	{
		void XBase::Bind(const char *szActionName)
		{
			Bind(Core::BlackBoard.Get<Action::XManager>()->GetByName(szActionName == NULL ? szName : szActionName));
		}

		void XBase::Bind(unsigned int uActionID)
		{
			Bind(Core::BlackBoard.Get<Action::XManager>()->GetByID(uActionID));
		}

		void XBase::Bind(Action::IBase *pAction)
		{
			if (m_pAction != pAction)
			{
				m_pAction = pAction;
			}
		}

		void XBase::Reset(const char *szName)
		{
			this->iID = 0;
			this->szName = szName;

			m_pAction = NULL;
			m_pEntity = NULL;
			m_pHandler = NULL;
			m_pMachine = NULL;
		}

		void XBase::RebindOwner()
		{
			if (m_pEntity == NULL)
			{
				return;
			}

			m_pHandler = m_pEntity->Require<Property::IHandler>();
			m_pMachine = m_pEntity->Require<State::XMachine>();
		}

		void XBase::SetOwner(Utils::Component::XBase *pEntity)
		{
			m_pEntity = pEntity;
		}

		Action::IBase *XBase::GetAction()
		{
			return m_pAction;
		}

		void XBase::InitValue(bool bPreCondition, const char *szName, bool bValue)
		{
			(*m_pHandler)(m_pAction, bPreCondition)->InitValue(szName, bValue);
		}

		void XBase::ApplyValue(const char *szName)
		{
			m_pMachine->DoAction(XMachine::EState::Current, szName, m_pAction);
		}

	}
}