#ifndef _GOAP_ACTION_XSTATIC_H_
#define _GOAP_ACTION_XSTATIC_H_

#include <utils-cpp/Base.h>

#include "AXBase.h"

namespace GOAP
{
	namespace Action
	{
		T(Property) class XStatic : public XBase<TProperty>
		{
		public:

			int GetProceduralCost()
			{
				return m_iCost;
			}
		};
	}
}

#endif