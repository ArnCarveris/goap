#include "AIBase.h"

#include "../Core/CXBlackBoard.h"

#include "AXManager.h"

namespace GOAP
{
	namespace Action
	{
		ImplementAccessor(IBase, Cost, int, m_iCost, );

		IBase::IBase()
		{
			Initialize(0);
		}
		
		void IBase::Copy(IBase *pAction)
		{
			Clone(pAction);

			Core::BlackBoard.Get < XManager >()->UnRegister(this);
		}

		void IBase::Copy(unsigned int iID)
		{
			Copy(Core::BlackBoard.Get < XManager >()->GetByID(iID));
		}

		void IBase::Copy(const char *szName)
		{
			Copy(Core::BlackBoard.Get < XManager >()->GetByName(szName));
		}

		void IBase::Initialize(const char *szName)
		{
			this->szName = szName;
			this->m_iCost = 0;
			this->iID = 0;
		}

		IBase::EConditionFlags IBase::GetFlags(EConditionFlags eFirst, EConditionFlags eSecond)
		{
			return (EConditionFlags)(eFirst | (eSecond << EConditionFlags::SecondShift));
		}

		IBase::EConditionFlags IBase::GetFlags(bool bFirstPreCondition, bool bSecondPreCondition)
		{
			return GetFlags(bFirstPreCondition ? EConditionFlags::Pre : EConditionFlags::Post, bSecondPreCondition ? EConditionFlags::Pre : EConditionFlags::Post);
		}

		bool IBase::GetConditionFromFlags(EConditionFlags eFlags, bool bSecond)
		{
			if (bSecond)
			{
				eFlags = (EConditionFlags)(eFlags >> EConditionFlags::SecondShift);
			}
			else
			{
				eFlags = (EConditionFlags)(eFlags & EConditionFlags::Pre);
			}

			return eFlags == EConditionFlags::Pre;
		}
	}
}