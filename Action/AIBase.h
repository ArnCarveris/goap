#ifndef _GOAP_ACTION_IBASE_H_
#define _GOAP_ACTION_IBASE_H_

#include <utils-cpp/Base.h>
#include <utils-cpp/XItem.h>

#include "AXSet.h"

namespace GOAP
{
	namespace Property
	{
		class IHandler;
	}

	namespace Action
	{
		/*
		Abstract pAction base class
		*/
		
		class IBase : public Utils::XItem
		{
		public:
			
			/*
			Condition flags used for apply, match, mask and etc. 
			*/

			enum EConditionFlags
			{
				None = 0,
				Pre = 1,
				Post = 2,
				All = 3,

				Second = 4,
				SecondShift = 2
			};

			IBase();

			void Initialize(const char *szName);

			static EConditionFlags GetFlags(EConditionFlags eFirst, EConditionFlags eSecond);

			static EConditionFlags GetFlags(bool bFirstPreCondition, bool bSecondPreCondition);

			static bool GetConditionFromFlags(Action::IBase::EConditionFlags eFlags, bool bSecond);

			/*
			Copy pAction from manager and unregister self
			*/
			
			void Copy(IBase *pAction);

			void Copy(unsigned int iID);

			void Copy(const char *szName);

			/*
			Abstract methods for inherited implementation 
			*/

			virtual int GetProceduralCost() = 0;
			
			virtual void Clone(IBase *pAction) = 0;

			DeclareAccessor(Cost, int, m_iCost, protected);
		};
		
		///<summary>
		/// Action creation
		///</summary>
		T(Action) TAction *Create(Property::IHandler *pHandler, const char *szName, XSet *pSet = NULL, int iCost = 1)
		{
			IBase *	pAction = new TAction();

			pAction->Initialize(szName);
			pAction->SetCost(iCost);

			pHandler->InitializeAction(pAction);

			if (pSet != NULL)
			{
				pSet->Add(pAction);
			}

			return (TAction*)pAction;
		}

		///<summary>
		/// Action creation
		///</summary>
		T(Action) TAction *Create(Utils::Component::XBase *entity, const char *szName, int iCost = 1)
		{
			Property::IHandler *pHandler = entity->Require<Property::IHandler>();

			return pHandler == NULL ? NULL : Create<TAction>(pHandler, szName, entity->Require<XSet>(), iCost);
		}

	}
	
}

#endif