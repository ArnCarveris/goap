#ifndef _GOAP_ACTION_XDELEGATE_H_
#define _GOAP_ACTION_XDELEGATE_H_

#include <utils-cpp/Base.h>
#include <utils-cpp/XDelegate.h>

#include "AXBase.h"

namespace GOAP
{
	namespace Action
	{
		T(Property) class XDelegate : public XBase<TProperty>
		{
		public:

			typedef Utils::Delegate::X1<int, IBase * > TDelegate;

			void SetDelegate(TDelegate & hDelegate)
			{
				m_hDelegate = hDelegate;
			}

			int GetProceduralCost()
			{
				return m_hDelegate(this);
			}

		protected:

			TDelegate m_hDelegate;
		};
	}
}

#endif