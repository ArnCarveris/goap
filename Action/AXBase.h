#ifndef _GOAP_ACTION_XBASE_H_
#define _GOAP_ACTION_XBASE_H_

#include <utils-cpp/Base.h>

#include "AIBase.h"

namespace GOAP
{
	namespace Action
	{
		T(Property) class XBase : public IBase
		{

		public:

			TProperty * GetCondition(bool bPreCondition)
			{
				return &hConditions[bPreCondition];
			}

			void Clone(IBase *pAction)
			{
				XBase *pSource = (XBase*)pAction;

				szName = pSource->szName;
				iID = pSource->iID;
				m_iCost = pSource->m_iCost;

				hConditions[0] = pSource->hConditions[0];
				hConditions[1] = pSource->hConditions[1];
			}

		protected:

			TProperty hConditions[2];
		};
	}
}

#endif