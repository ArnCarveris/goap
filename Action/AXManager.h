#ifndef _GOAP_ACTION_XMANAGER_H_
#define _GOAP_ACTION_XMANAGER_H_

#include <utils-cpp/XDictionary.h>
#include <utils-cpp/XManager.h>

#include "../Core/CIManager.h"

#include "AIBase.h"
#include "AXSet.h"

namespace GOAP
{
	namespace Action
	{
		class XManager : public Utils::XManager<Action::IBase*, unsigned int>, public Core::IManager
		{
		protected:

			Action::IBase *GetByItemSet(unsigned int iID)
			{
				return iID > 0 ? *items.GetAtIndex(iID - 1) : NULL;
			}
		};
	}
}

#endif