#include "AXSet.h"

#include <utils-cpp/XComponentBase.h>

#include "../Core/CXBlackBoard.h"
#include "../Property/PIHandler.h"

#include "AXManager.h"

namespace GOAP
{
	namespace Action
	{

		Action::IBase * XSet::operator()(XManager *pManager, int iIndex)
		{
			return pManager->GetFromSetAtIndex(this, iIndex);
		}

		void XSet::Add(Action::IBase *pAction)
		{
			if (pAction == NULL)
			{
				return;
			}

			unsigned int iID = pAction->GetID();

			if (iID == 0)
			{
				Core::BlackBoard.Get<Action::XManager>()->Register(pAction);
			}

			iID = pAction->GetID();

			if (iID > 0)
			{
				Utils::XIDSet::Add(iID, true);
			}
		}
	}
}