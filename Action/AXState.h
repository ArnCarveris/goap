#ifndef _GOAP_ACTION_XSTATE_H_
#define _GOAP_ACTION_XSTATE_H_

#include <utils-cpp/Base.h>

#include "AXBase.h"

namespace GOAP
{
	namespace Action
	{
		T(Property) class XState : public XBase<TProperty>
		{
		public:

			int GetProceduralCost()
			{
				return 0;
			}
		};

		///<summary>
		/// State creation based on action
		///</summary>
		T(Property) IBase *CreateState(Property::IHandler *pHandler, const char *szName)
		{
			return Create< XState < TProperty > >(pHandler, szName, NULL, 0);
		}
		
		///<summary>
		/// State creation based on action
		///</summary>
		T(Property) IBase *CreateState(Utils::Component::XBase *entity, const char *szName)
		{
			return Create< XState < TProperty > >(entity->Require<Property::IHandler>(), szName, NULL, 0);
		}
	}
}

#endif