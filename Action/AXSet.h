#ifndef _GOAP_ACTION_XSET_H_
#define _GOAP_ACTION_XSET_H_

#include <utils-cpp/XSet.h>
#include <utils-cpp/XComponentNull.h>

namespace GOAP
{
	namespace Action
	{
		class IBase;
		class XManager;
		class XSet : public Utils::XIDSet, public Utils::Component::XNull
		{
		public:

			Action::IBase * operator ()(XManager *pManager, int iIndex);

			void Add(Action::IBase *pAction);
		};
	}
}

#endif