#include "BIMachine.h"

namespace GOAP
{
	namespace Balance
	{
		void IMachine::Initialize(Utils::Component::XBase *entity)
		{
			entity->RegisterMethod<IMachine, &IMachine::OnInitialize >("OnInitialize", this);
		}
	}
}