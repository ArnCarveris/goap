#ifndef _GOAP_BALANCE_PROPERTY_XMACHINE_H_
#define _GOAP_BALANCE_PROPERTY_XMACHINE_H_

#include <vector>

#include "../BIMachine.h"

namespace GOAP
{
	namespace Property
	{
		class XSet;
	}

	namespace Balance
	{
		namespace Property
		{
			class XSet;
			class XNode;
			class XMachine : public IMachine
			{
			public:

				XMachine();
				~XMachine();

				void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args);

				void Update();

				void Update(Utils::Component::XBase *entity);

				void Shutdown(Utils::Component::XBase *entity);

				XNode& GetNode(const char *szName) const;

			protected:

				XSet *					m_pNodes; //< Node set component

				GOAP::Property::XSet *	m_pContext; //< Properties atoms context component
			};
		}
	}
}

#endif