#include "BPXMachine.h"
#include "BPXNode.h"
#include "BPXSet.h"

#include "../../Property/PXSet.h"

namespace GOAP
{
	namespace Balance
	{
		namespace Property
		{

			XMachine::XMachine() : m_pContext(NULL), m_pNodes(NULL) { }
			
			XMachine::~XMachine()
			{
				m_pNodes = NULL;
				m_pContext = NULL;
			}

			void XMachine::OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
			{
				m_pContext = entity->Require<GOAP::Property::XSet>();
				m_pNodes = entity->Require<XSet>();
			}
			
			void XMachine::Update()
			{
				m_pNodes->Clear();

				for (size_t i = 0, l = m_pNodes->GetSize(); i < l; i++)
				{
					m_pNodes->Add(XNode());
				}
			}
			
			void XMachine::Update(Utils::Component::XBase *entity)
			{
				OnInitialize(entity, NULL);
				
				Update();
			}
			
			void XMachine::Shutdown(Utils::Component::XBase *entity)
			{
				m_pContext = NULL;
				m_pNodes = NULL;
			}

			XNode& XMachine::GetNode(const char *szName) const
			{
				return (*m_pNodes)[m_pContext->GetAtomIndex(szName)];
			}
		}
	}
}