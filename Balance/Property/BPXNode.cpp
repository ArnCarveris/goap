#include "BPXNode.h"

#include <math.h>

namespace GOAP
{
	namespace Balance
	{
		namespace Property
		{
			XNode::XNode() : m_uCost(1), m_iValue(0), m_iCondition(0) { }

			void XNode::SetCost(unsigned int uCost)
			{
				m_uCost = uCost;
			}

			unsigned int XNode::GetCost()
			{
				return m_uCost;
			}

			unsigned int XNode::GetCost(bool bCondition, bool bValue)
			{
				if (m_uCost == 0)
				{
					return 0;
				}

				unsigned int	uCost = m_uCost;

				AppyCostBalance(uCost, m_iCondition, bCondition);

				AppyCostBalance(uCost, m_iValue, bValue);

				if (uCost < 1)
				{
					uCost = 1;
				}
				
				return uCost;
			}

			void XNode::SetValue(char iValue)
			{
				LimitBalance(iValue);

				m_iValue = iValue;
			}

			char XNode::GetValue()
			{
				return m_iValue;
			}

			void XNode::SetCondition(char iCondition)
			{
				LimitBalance(iCondition);

				m_iCondition = iCondition;
			}

			char XNode::GetCondition()
			{
				return m_iCondition;
			}

			bool XNode::operator == (const XNode &other) const
			{
				return m_iCondition == other.m_iCondition && m_iValue == other.m_iValue && m_uCost == other.m_uCost;
			}

			bool XNode::operator != (const XNode &other) const
			{
				return m_iCondition != other.m_iCondition || m_iValue != other.m_iValue || m_uCost != other.m_uCost;
			}

			void XNode::LimitBalance(char &balance)
			{
				if (balance < -100)
				{
					balance = -100;
				}
				else if (balance > 100)
				{
					balance = 100;
				}
			}

			void XNode::AppyCostBalance(unsigned int &cost, char &balance, bool &value)
			{
				if (value ? balance < 0 : balance > 0)
				{
					cost = (unsigned int)((float)cost * (1.0f - abs((float)balance)) / 100.0f);
				}
			}
		}
	}
}