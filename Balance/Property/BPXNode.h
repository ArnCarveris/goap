#ifndef _GOAP_BALANCE_PROPERTY_XNODE_H
#define _GOAP_BALANCE_PROPERTY_XNODE_H

namespace GOAP
{
	namespace Balance
	{
		namespace Property
		{
			class XNode
			{
			public:

				XNode();

				///<summary>
				///Sets base cost
				///</summary>
				void SetCost(unsigned int uCost);

				///<summary>
				///Gets base cost
				///</summary>
				unsigned int GetCost();

				///<summary>
				///Gets balanced cost 
				///</summary>
				unsigned int GetCost(bool bCondition, bool bValue);

				///<summary>
				///Sets value balance within range [-100..100]
				///</summary>
				void SetValue(char iValue);
				
				///<summary>
				///Gets value balance within range [-100..100]
				///</summary>
				char GetValue();

				///<summary>
				///Sets condition balance within range [-100..100]
				///</summary>
				void SetCondition(char iCondition);

				///<summary>
				///Gets condition balance within range [-100..100]
				///</summary>
				char GetCondition();

				bool operator==(const XNode &other) const;

				bool operator!=(const XNode &other) const;
			protected:

				///<summary>
				///Limits balance with range [-100..100]
				///</summary>
				void LimitBalance(char &balance);

				///<summary>
				///Apply balance to cost at value
				///</summary>
				void AppyCostBalance(unsigned int &cost, char &balance, bool &value);

				unsigned int	m_uCost; //< lower better but greater than 0, overwise ignores 
				char			m_iValue; //< balance within range [-100..100] for [false..true] value
				char			m_iCondition; //< balance within range [-100...100] for [pre..post] condition ()
			};
		}
	}
}

#endif