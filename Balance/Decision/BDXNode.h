#ifndef _GOAP_BALANCE_DECISION_XNODE_H_
#define _GOAP_BALANCE_DECISION_XNODE_H_

namespace GOAP
{
	namespace Balance
	{
		namespace Decision
		{
			class XNode
			{
			public:

				XNode();
				XNode(float fValue);

				void SetValue(float fValue);

				float GetValue();

				bool operator==(const XNode &other) const;

				bool operator!=(const XNode &other) const;

			protected:

				float m_fValue; // < Balanced decision value withing range [-1..1]
			};
		}
	}
}

#endif