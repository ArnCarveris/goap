#ifndef _GOAP_BALANCE_DECISION_XMACHINE_H_
#define _GOAP_BALANCE_DECISION_XMACHINE_H_

#include <utils-cpp/IComponentBase.h>
#include <utils-cpp/XComponentBase.h>

#include "../BIMachine.h"

namespace GOAP
{
	namespace Decision
	{
		class XSet;
		class XManager;
	}

	namespace Balance
	{
		namespace Decision
		{
			class XSet;
			class XMachine : public IMachine
			{
			public:

				XMachine();
				~XMachine();

				void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args);

				void Update();
				
				void Update(Utils::Component::XBase *entity);

				void Shutdow(Utils::Component::XBase *entity);
			
			protected:

				XSet *						m_pNodes;

				GOAP::Decision::XSet *		m_pDecisions;
				GOAP::Decision::XManager *	m_pManager;
			};
		}
	}
}

#endif