#ifndef _GOAP_BALANCE_DECISION_XSET_H_
#define _GOAP_BALANCE_DECISION_XSET_H_

#include <utils-cpp/XSet.h>
#include <utils-cpp/XComponentNull.h>

#include "BDXNode.h"

namespace GOAP
{
	namespace Balance
	{
		namespace Decision
		{
			class XSet : public Utils::XSet<XNode>, public Utils::Component::XNull { };
		}
	}
}

#endif