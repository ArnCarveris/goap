#include "BDXNode.h"

namespace GOAP
{
	namespace Balance
	{
		namespace Decision
		{
			XNode::XNode() : m_fValue(0.0f) { }

			XNode::XNode(float fValue)
			{
				m_fValue = fValue;
			}

			void XNode::SetValue(float fValue)
			{
				m_fValue = fValue;
			}

			float XNode::GetValue()
			{
				return m_fValue;
			}

			bool XNode::operator==(const XNode &other) const
			{
				return m_fValue == other.m_fValue;
			}

			bool XNode::operator!=(const XNode &other) const
			{
				return m_fValue != other.m_fValue;
			}
		}
	}
}