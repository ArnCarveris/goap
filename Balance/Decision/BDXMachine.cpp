#include "BDXMachine.h"

#include "BDXNode.h"
#include "BDXSet.h"

#include "../../Decision/DXSet.h"
#include "../../Decision/DXManager.h"
#include "../../Decision/DXStatic.h"

#include "../../Core/CXBlackBoard.h"

namespace GOAP
{
	namespace Balance
	{
		namespace Decision
		{
			XMachine::XMachine() : m_pDecisions(NULL), m_pNodes(NULL)
			{
				m_pManager = GOAP::Core::BlackBoard.Get<GOAP::Decision::XManager>();
			}

			XMachine::~XMachine()
			{
				m_pDecisions = NULL;
				m_pManager = NULL;
				m_pNodes = NULL;
			}

			void XMachine::OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
			{
				m_pDecisions = entity->Require<GOAP::Decision::XSet>();
				m_pNodes = entity->Require<XSet>();
			}

			void XMachine::Update()
			{
				m_pNodes->Clear();

				for (size_t i = 0, l = m_pDecisions->GetSize(); i < l; i++)
				{
					m_pNodes->Add(XNode((*m_pDecisions)(m_pManager, (int)i)->CalculateUtility()));
				}
			}

			void XMachine::Update(Utils::Component::XBase *entity)
			{
				OnInitialize(entity, NULL);

				Update();
			}

			void XMachine::Shutdow(Utils::Component::XBase *entity)
			{
				m_pDecisions = NULL;
				m_pManager = NULL;
				m_pNodes = NULL;
			}
		}
	}
}