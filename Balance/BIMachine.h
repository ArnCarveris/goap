#ifndef _GOAP_BALANCE_IMACHINE_H_
#define _GOAP_BALANCE_IMACHINE_H_

#include <utils-cpp/IComponentBase.h>
#include <utils-cpp/XComponentBase.h>

namespace GOAP
{
	namespace Balance
	{
		class IMachine : public Utils::Component::IBase  
		{
		public:

			void Initialize(Utils::Component::XBase *entity);

			virtual void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args) = 0;

			virtual void Update() = 0;
		};
	}
}

#endif