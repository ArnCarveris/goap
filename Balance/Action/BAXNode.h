#ifndef _GOAP_BALANCE_ACTION_XNODE_H_
#define _GOAP_BALANCE_ACTION_XNODE_H_

namespace GOAP
{
	namespace Balance
	{
		namespace Action
		{
			struct XNode
			{
				unsigned int cost;

				bool operator==(const XNode &other) const
				{
					return cost == other.cost;
				}

				bool operator!=(const XNode &other) const
				{
					return cost != other.cost;
				}
			};
		}
	}
}
#endif