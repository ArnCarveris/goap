#ifndef _GOAP_BALANCE_ACTION_XSET_H_
#define _GOAP_BALANCE_ACTION_XSET_H_

#include <utils-cpp/XSet.h>
#include <utils-cpp/XComponentNull.h>

#include "BAXNode.h"

namespace GOAP
{
	namespace Balance
	{
		namespace Action
		{
			class XSet : public Utils::XSet<XNode>, public Utils::Component::XNull { };
		}
	}
}

#endif