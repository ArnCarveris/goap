#ifndef _GOAP_BALANCE_ACTION_XMACHINE_H_
#define _GOAP_BALANCE_ACTION_XMACHINE_H_

#include <vector>

#include "../BIMachine.h"

namespace GOAP
{
	namespace Action
	{
		class XSet;
		class XManager;
	}

	namespace Balance
	{
		namespace Action
		{
			class XSet;
			class XMachine : public IMachine
			{
			public:

				XMachine();
				~XMachine();

				void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args);

				void Update();

				void Update(Utils::Component::XBase *entity);
				
				void Shutdown(Utils::Component::XBase *entity);

			protected:
				XSet *						m_pNodes; //< Balanced action cost set
				GOAP::Action::XSet *		m_pActions; //< Current action set component
				GOAP::Action::XManager *	m_pManager; //< Action manager instance pointer from blackboard
			};
		}
	}
}

#endif