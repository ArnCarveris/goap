#include "BAXMachine.h"
#include "BAXNode.h"
#include "BAXSet.h"

#include "../../Action/AIBase.h"
#include "../../Action/AXSet.h"

#include "../../Core/CXBlackBoard.h"

namespace GOAP
{
	namespace Balance
	{
		namespace Action
		{
			XMachine::XMachine() : m_pActions(NULL), m_pNodes(NULL)
			{
				m_pManager = GOAP::Core::BlackBoard.Get<GOAP::Action::XManager>();
			}

			XMachine::~XMachine()
			{
				m_pNodes = NULL;
				m_pActions = NULL;
				m_pManager = NULL;
			}

			void XMachine::OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
			{
				m_pActions = entity->Require<GOAP::Action::XSet>();
				m_pNodes = entity->Require<XSet>();
			}

			void XMachine::Update()
			{
				m_pNodes->Clear();

				for (size_t i = 0, l = m_pActions->GetSize(); i < l; i++)
				{
					XNode hNode = { (*m_pActions)(m_pManager, (int)i)->GetCost() };

					m_pNodes->Add(hNode);
				}
			}

			void XMachine::Update(Utils::Component::XBase *entity)
			{
				OnInitialize(entity, NULL);

				Update();
			}

			void XMachine::Shutdown(Utils::Component::XBase *entity)
			{
				m_pNodes = NULL;
				m_pManager = NULL;
				m_pActions = NULL;
			}
		}
	}
}