#ifndef _GOAP_PROPERTY_XCONTEXT_H_
#define _GOAP_PROPERTY_XCONTEXT_H_

#include <utils-cpp/XSet.h>
#include <utils-cpp/Base.h>

#include <utils-cpp/XComponentNull.h>

namespace GOAP
{
	namespace Property
	{
		/*
		Property set is context used to map property atoms, wire name with index
		*/
		class XSet : public Utils::XNameSet, public Utils::Component::XNull
		{
		public:

			/*
			Constructor with limit argument, if limit is 0, then there aren't any atoms count cap.
			*/
			XSet();

			XSet(unsigned int uLimit);
			
			~XSet();

			/*
			Atom name, index, limit and count accessors
			*/

			const char *GetAtomAtIndex(int iIndex);

			int GetAtomIndex(const char *szMame);

			unsigned int GetAtomsCount();

			DeclareAccessor(AtomsLimit, unsigned int, m_uLimit, protected);
		};
	}
}


#endif