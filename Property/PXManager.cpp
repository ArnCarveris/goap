#include "PXManager.h"
#include "PXSet.h"

namespace GOAP
{
	namespace Property
	{
		Property::XSet * XManager::GetNamedSet(const char *szName, unsigned int uLimit)
		{
			XSet * pSet = (Utils::XManager<IHandler*, const char*>::GetNamedSet<XSet>(szName));

			if (pSet->GetAtomsLimit() < 1)
			{
				pSet->SetAtomsLimit(uLimit);
			}

			return pSet;
		}
	}
}