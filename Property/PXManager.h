#ifndef _GOAP_PROPERTY_XMANAGER_H_
#define _GOAP_PROPERTY_XMANAGER_H_

#include <utils-cpp/XManager.h>

#include "../Core/CIManager.h"

#include "PIHandler.h"

namespace GOAP
{
	namespace Property
	{
		class XSet;
		class XManager : public Utils::XManager<IHandler*, const char*>, public Core::IManager
		{
		public:

			XSet * GetNamedSet(const char *szName, unsigned int uLimit);

		protected:

			IHandler *GetByItemSet(const char *szItem)
			{
				return NULL;
			}
		};
	}
}

#endif