#ifndef _GOAP_PROPERTY_XHANDLER_H_
#define _GOAP_PROPERTY_XHANDLER_H_

#include <utils-cpp/Base.h>

#include "../Action/AXBase.h"
#include "../Action/AXState.h"
#include "../Property/PIHandler.h"
#include "../Property/PXSet.h"

namespace GOAP
{
	namespace Property
	{
		T(Property) class XHandler : public IHandler
		{
		public:

			XHandler* operator[] (TProperty *pProperty)
			{
				m_pProperty = pProperty;

				return this;
			}

			IHandler* operator() (Action::IBase *pAction, Action::IBase::EConditionFlags eFlags)
			{
				const bool bPreCondition = eFlags == Action::IBase::EConditionFlags::Pre;

				m_pProperty = ((Action::XBase<TProperty>*)pAction)->GetCondition(bPreCondition);

				return this;
			}

			void Apply(TProperty *pSource, const char *szName = NULL)
			{
				Apply(m_pProperty, pSource, szName);
			}

			void Apply(Action::IBase::EConditionFlags eFlags, Action::IBase *pState, Action::IBase *pAction, const char *szName = NULL)
			{
				TProperty *pProperties[2];

				pProperties[0] = ((Action::XBase<TProperty>*)pState)->GetCondition(Action::IBase::GetConditionFromFlags(eFlags, false));
				pProperties[1] = ((Action::XBase<TProperty>*)pAction)->GetCondition(Action::IBase::GetConditionFromFlags(eFlags, true));

				Apply(pProperties[0], pProperties[1], szName);
			}

			bool Match(Action::IBase::EConditionFlags eFlags, Action::IBase *pState, Action::IBase *pAction, bool bFromCare = false)
			{
				TProperty hProperties[2];

				hProperties[0] = *((Action::XBase<TProperty>*)pState)->GetCondition(Action::IBase::GetConditionFromFlags(eFlags, false));
				hProperties[1] = *((Action::XBase<TProperty>*)pAction)->GetCondition(Action::IBase::GetConditionFromFlags(eFlags, true));

				return (*this)[&hProperties[0]]->Match(&hProperties[1], bFromCare);
			}


			Action::IBase *CreateState(const char *szName)
			{
				return Action::CreateState<TProperty>(this, szName);
			}

			virtual bool Match(TProperty *pProperty, bool bFromCare = false) = 0;

			virtual bool Equals(TProperty *pProperty) = 0;

			virtual void Copy(TProperty *pProperty) = 0;

			virtual int CalculateH(TProperty *pProperty) = 0;

			virtual void Apply(TProperty *pDestination, TProperty *pSource, const char *szName = NULL) = 0;

		protected:



			TProperty *m_pProperty;

		};
	}
}

#endif