#include "PDXHandler.h"

#define ToString(_value_) #_value_
#define ForContext for (int c = m_pContext->GetAtomsCount(), l = m_pContext->GetAtomsLimit(), i = 0; (l < 1 || i < l) && i < c ; ++i)

namespace GOAP
{
	namespace Property
	{
		namespace Dictionary
		{
			XHandler::XHandler()
			{
				SetName(ToString(GOAP::Property::Dictionary::XHandler));
			}

			//
			// Atoms accessors
			//
			

			bool XHandler::InitValue(const char *szName, bool bValue)
			{
				return m_pProperty->Add(GetIndex(szName), bValue);
			}


			bool XHandler::SetValue(const char *szName, bool bValue)
			{
				m_pProperty->Set(GetIndex(szName), bValue);
				
				return true;

			}

			
			bool XHandler::GetValue(const char *szName)
			{
				return *m_pProperty->Get(GetIndex(szName));
			}

			void XHandler::IncludeMask(void *pMask)
			{
				long long *llMask = (long long*)(pMask);

				ForContext
				{
					if (m_pProperty->Has(i))
					{
						(*llMask) |= (long long)(1LL << i);
					}
				}
			}

			//
			// Printing
			//	

			void XHandler::Print(TDelegate & hCallback)
			{
				ForContext
				{
					if (m_pProperty->Has(i))
					{
						hCallback(i, m_pContext->GetAtomAtIndex(i), *m_pProperty->Get(i));
					}
				}
			}

			//
			// Action methods
			//

			void XHandler::InitializeAction(GOAP::Action::IBase *pAction)
			{
				Clear(pAction);
			}

			void XHandler::Clear(GOAP::Action::IBase *pAction)
			{
				ForBool(PreCondition, ((GOAP::Action::XBase<XBase>*)pAction)->GetCondition(bPreCondition)->Clear(););
			}

			void XHandler::Clear()
			{
				m_pProperty->Clear();
			}
			//
			// Property Operations
			//

			bool XHandler::Match(XBase *pProperty, bool bFromCare)
			{
				XBase *pProperties[2];

				if (bFromCare)
				{
					pProperties[0] = pProperty;
					pProperties[1] = m_pProperty;
				}
				else
				{
					pProperties[0] = m_pProperty;
					pProperties[1] = pProperty;
				}

				for (int c = pProperties[1]->GetCount(), i = 0, k; i < c; i++)
				{
					k = pProperties[1]->GetKeyAtIndex(i);

					if (pProperties[0]->Has(k) == false || *pProperties[0]->Get(k) != *pProperties[1]->Get(k))
					{
						return false;
					}

				}

				return true;
			}


			bool XHandler::Equals(XBase *pProperty)
			{
				const int iCount = m_pProperty->GetCount();

				if (iCount != pProperty->GetCount())
				{
					return false;
				}

				for (int i = 0, k; i < iCount; i++)
				{
					k = m_pProperty->GetKeyAtIndex(i);

					if (pProperty->Has(k) == false || *m_pProperty->Get(k) != *pProperty->Get(k))
					{
						return false;
					}
				}

				return true;
			}

			void XHandler::Copy(XBase *pProperty)
			{
				m_pProperty->Copy(*pProperty);
			}


			void XHandler::Apply(XBase *pDestination, XBase *pSource, const char *szName)
			{
				if (szName == NULL)
				{
					for (int i = 0, k, c = pSource->GetCount(); i < c; i++)
					{
						k = pSource->GetKeyAtIndex(i);

						pDestination->Set(k, *pSource->Get(k));
					}
				}
				else
				{
					pDestination->Set(GetIndex(szName), *pSource->Get(GetIndex(szName)));
				}
			}

			int XHandler::CalculateH(XBase *pProperty)
			{
				int iCount[2] = {
					m_pProperty->GetCount(),
					pProperty->GetCount()
				};

				int iDist = 0;

				XBase *pProperties[2];

				if (iCount[0] < iCount[1])
				{
					pProperties[0] = pProperty;
					pProperties[1] = m_pProperty;
				}
				else
				{
					pProperties[0] = m_pProperty;
					pProperties[1] = pProperty;
				}

				iCount[0] = pProperties[0]->GetCount();
				iCount[1] = pProperties[1]->GetCount();

				for (int i = 0, k; i < iCount[0]; ++i)
				{
					k = pProperties[0]->GetKeyAtIndex(i);

					if (pProperties[1]->Has(k) && *pProperties[0]->Get(k) != *pProperties[1]->Get(k))
					{
						iDist += GetCost(true, i, *pProperties[1]->Get(k));
					}
				}

				return iDist;
			} 


			int XHandler::GetIndex(const char* szName)
			{
				return m_pContext->GetAtomIndex(szName);
			}
		}
	}
}

#undef ToString
#undef ForContext