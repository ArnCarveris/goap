#ifndef _GOAP_PROPERTY_DICTIONARY_XHANDLER_H_
#define _GOAP_PROPERTY_DICTIONARY_XHANDLER_H_

#include <utils-cpp/XDictionary.h>

#include "../PXHandler.h"


namespace GOAP
{
	namespace Property
	{
		namespace Dictionary
		{
			typedef Utils::XDictionary<int, bool> XBase;

			class XHandler : public Property::XHandler < XBase >
			{
			public:

				XHandler();

				//
				// Atoms accessors
				//

				bool InitValue(const char *szName, bool bValue);

				bool SetValue(const char *szName, bool bValue);

				bool GetValue(const char *szName);

				void IncludeMask(void *pMask);

				//
				// Printing
				//	

				void Print(TDelegate & hCallback);


				//
				// Action methods
				//

				void InitializeAction(GOAP::Action::IBase *pAction);

				void Clear(GOAP::Action::IBase *pAction);

				void Clear();

				//
				// Property Operations
				//

				bool Match(XBase *pProperty, bool bFromCare = false);

				bool Equals(XBase *pProperty);

				void Copy(XBase *pProperty);

				void Apply(XBase *destination, XBase *source, const char *szName = NULL);

				int CalculateH(XBase *pProperty);

			protected:

				int GetIndex(const char* szName);
			};
		}
	}
}

#endif