#include "PIHandler.h"
#include "PXSet.h"
#include "../Balance/Property/BPXSet.h"

namespace GOAP
{
	namespace Property
	{
		Property::IHandler* IHandler::operator() (Action::IBase *pAction, bool bPreCondition)
		{
			return operator()(pAction, bPreCondition ? Action::IBase::EConditionFlags::Pre : Action::IBase::EConditionFlags::Post);
		}

		void IHandler::Initialize(Utils::Component::XBase *entity)
		{
			m_hAtomCost[0] = TAtomCostDelegate::FromMethod < IHandler, &IHandler::GetNormalCost > (this);
			m_hAtomCost[1] = TAtomCostDelegate::FromMethod < IHandler, &IHandler::GetBalancedCost > (this);

			DisableBalancer();

			entity->RegisterMethod<IHandler, &IHandler::OnInitialize >("OnInitialize", this);
		}

		void IHandler::OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
		{
			m_pContext = entity->Require<XSet>();
			m_pBalance = entity->Require<Balance::Property::XSet>();

			EnableBalancer();
		}

		void IHandler::Update(Utils::Component::XBase *entity)
		{
			OnInitialize(entity, NULL);
		}

		void IHandler::Shutdown(Utils::Component::XBase *entity)
		{
			m_pBalance = NULL;
			m_pContext = NULL;

			DisableBalancer();
		}

		void IHandler::EnableBalancer()
		{
			m_bUseBalancer = m_pBalance != NULL;
		}

		void IHandler::DisableBalancer()
		{
			m_bUseBalancer = NULL;
		}

		int IHandler::GetCost(bool bPreCondition, const char *szName, bool bValue)
		{
			return GetCost(bPreCondition, m_pContext->GetAtomIndex(szName), bValue);
		}

		int IHandler::GetCost(bool bPreCondition, int iAtomIndex, bool bValue)
		{
			return m_hAtomCost[m_bUseBalancer](bPreCondition, iAtomIndex, bValue);
		}

		int IHandler::GetNormalCost(bool bPreCondition, int iAtomIndex, bool bValue)
		{
			return 1;
		}

		int IHandler::GetBalancedCost(bool bPreCondition, int iAtomIndex, bool bValue)
		{
			return (int)((*m_pBalance)[iAtomIndex].GetCost(!bPreCondition, bValue));
		}
	}
}