#include "PXSet.h"

#include <string.h>

namespace GOAP
{
	namespace Property
	{
		ImplementAccessor(XSet, AtomsLimit, unsigned int, m_uLimit, )

		XSet::XSet()
		{
			SetAtomsLimit(0);
		}

		XSet::XSet(unsigned int uLimit)
		{
			SetAtomsLimit(uLimit);
		}

		XSet::~XSet()
		{
			Clear();
		}

		const char *XSet::GetAtomAtIndex(int iIndex)
		{
			return items[iIndex];
		}

		int XSet::GetAtomIndex(const char *szName)
		{
			int iIdx = 0;

			for (size_t c = items.size(); (size_t)iIdx < c; ++iIdx)
			{
				if (!strcmp(items[iIdx], szName)) return iIdx;
			}

			if (m_uLimit < 1 || (unsigned int)iIdx < m_uLimit)
			{
				items.push_back(szName);
			}
			else
			{
				iIdx = -1;
			}

			return iIdx;
		}

		unsigned int XSet::GetAtomsCount()
		{
			return GetSize();
		}
	}
}