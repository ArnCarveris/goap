#ifndef _GOAP_PROPERTY_BIT_FIELD_XBASE_H_
#define _GOAP_PROPERTY_BIT_FIELD_XBASE_H_

#include <utils-cpp/Base.h>

namespace GOAP
{
	namespace Property
	{
		namespace BitField
		{
			T(Data) struct XBase //!< Describes listing values (t/f) for all known atoms.
			{
				TData values;	//!< Values for atoms.
				TData dontcare;	//!< Mask for atoms that do not matter.
			};
		}
	}
}

#endif