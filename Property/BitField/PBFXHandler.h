#ifndef _GOAP_PROPERTY_BIT_FIELD_XHANDLER_H_
#define _GOAP_PROPERTY_BIT_FIELD_XHANDLER_H_

#include <utils-cpp/Base.h>

#include "../PXHandler.h"

#include "PBFXBase.h"

#undef GetAtom
#undef SetAtom
#undef HasAtom
#undef DoCareAtom
#undef DoAtom

#define GetAtom(iIdx) ( (TData)1 << iIdx )
#define SetAtom(iIdx, flag) ( flag * GetAtom(iIdx))

#define HasAtom(iIdx, bValue) (bValue & GetAtom(iIdx))

#define DoCareAtom(pState) ( pState.dontcare ^ (TData)(-1) )
#define DoAtom(hCare, first, op ,second)  ( ( first.values & hCare ) op ( second.values & hCare ) )

#define ToString(_value_) #_value_

namespace GOAP
{
	namespace Property
	{
		namespace BitField
		{
			T(Data) class XHandler : public Property::XHandler < XBase < TData > >
			{
			public:

				typedef XBase<TData> TProperty;

				XHandler()
				{
					SetName(ToString(GOAP::Property::BitField::XHandler<TProperty>));
				}

				//
				// Atoms accessors
				//

				bool InitValue(const char *szName, bool bValue)
				{
					return SetAtomValue(m_pProperty, szName, bValue);
				}

				bool SetValue(const char *szName, bool bValue)
				{
					return DoApplyValue(m_pProperty, szName, bValue);
				}

				bool GetValue(const char *szName)
				{
					return GetAtomValue(m_pProperty, szName);
				}

				void IncludeMask(void *pMask)
				{
					long long *llMask = (long long*)(pMask);

					for (int i = 0, c = m_pContext->GetAtomsCount(), l = m_pContext->GetAtomsLimit(); i < l && i < c; ++i)
					{
						if (HasAtom(i, m_pProperty->dontcare) == (TData)(0))
						{
							(*llMask) |= (long long)((TData)(1) << i);
						}
					}
				}

				//
				// Printing
				//	

				void Print(TDelegate & hCallback)
				{
					for (int i = 0, c = m_pContext->GetAtomsCount(), l = m_pContext->GetAtomsLimit(); i < l && i < c; ++i)
					{
						if (HasAtom(i, m_pProperty->dontcare) == 0LL)
						{
							hCallback(i, m_pContext->GetAtomAtIndex(i), HasAtom(i, m_pProperty->values) != 0LL);
						}
					}
				}

				//
				// Action methods
				//

				void InitializeAction(GOAP::Action::IBase *pAction)
				{
					Clear(pAction);
				}

				void Clear(GOAP::Action::IBase *pAction)
				{
					ForBool(PreCondition, DoClear(((GOAP::Action::XBase<TProperty>*)pAction)->GetCondition(bPreCondition)););
				}

				void Clear()
				{
					DoClear(m_pProperty);
				}

				//
				// Property Operations
				//

				bool Match(TProperty *pProperty, bool bFromCare = false)
				{
					return DoMatch(*m_pProperty, *pProperty, bFromCare);
				}

				bool Equals(TProperty *pProperty)
				{
					return m_pProperty->values == pProperty->values;
				}

				void Copy(TProperty *pProperty)
				{
					m_pProperty->values = pProperty->values;
					m_pProperty->dontcare = pProperty->dontcare;
				}

				void Apply(TProperty *pDestination, TProperty *pSource, const char *szName = NULL)
				{
					TProperty hMasked = *pSource;

					if (szName != NULL)
					{
						TProperty hCondition;

						DoClear(&hCondition);

						SetAtomValue(&hCondition, szName, operator[](pSource)->GetValue(szName));

						hMasked.values &= hCondition.values;
						hMasked.dontcare &= hCondition.dontcare;
					}

					DoApply(pDestination, &hMasked);

				}
				
				int CalculateH(TProperty *pProperty)
				{
					const TData hCare = DoCareAtom((*pProperty));
					const TData hDiff = DoAtom(hCare, (*m_pProperty), ^, (*pProperty));
					int iDist = 0;

					for (int i = 0, l = m_pContext->GetAtomsLimit(); i < l; ++i)
					{
						if (HasAtom(i, hDiff) != 0)
						{
							iDist += GetCost(true, i, GetAtomValue(pProperty, i));
						}
					}

					return iDist;
				}

			protected:

				//
				// Atom methods
				//
				bool SetAtomValue(TProperty *pState, int iAtomIdx, bool bValue)
				{
					if (ValidateIndex(iAtomIdx, m_pContext->GetAtomsLimit()) == false)
					{
						return false;
					}

					pState->values |= SetAtom(iAtomIdx, bValue);
					pState->dontcare ^= GetAtom(iAtomIdx);

					return true;
				}

				bool GetAtomValue(TProperty *pState, int iAtomIdx)
				{
					return ValidateIndex(iAtomIdx, m_pContext->GetAtomsLimit()) && HasAtom(iAtomIdx, pState->dontcare) == 0LL && HasAtom(iAtomIdx, pState->values) != 0LL;
				}

				bool GetAtomValue(TProperty *pState, const char *szAtomName)
				{
					return GetAtomValue(pState, m_pContext->GetAtomIndex(szAtomName));
				}

				bool SetAtomValue(TProperty *pState, const char *szAtomName, bool bValue)
				{
					return SetAtomValue(pState, m_pContext->GetAtomIndex(szAtomName), bValue);
				}

				//
				// Property Operations
				//
				
				bool DoMatch(TProperty hFrom, TProperty hTo, bool bFromCare)
				{
					const TData hCare = DoCareAtom((bFromCare ? hFrom : hTo));

					return DoAtom(hCare, hFrom, == , hTo);
				}

				void DoClear(TProperty* pState)
				{
					pState->values = (TData)0;
					pState->dontcare = (TData)-1;
				}

				bool DoCare(TProperty *pState, int iIdx)
				{
					return HasAtom(iIdx, pState->dontcare) == 0LL;
				}

				void DoApply(TProperty *pDestination, TProperty *pSource)
				{
					const TData hUnaffected = pSource->dontcare;
					const TData hAffected = (hUnaffected ^ -1LL);

					pDestination->values = (pDestination->values & hUnaffected) | (pSource->values & hAffected);
					pDestination->dontcare &= pSource->dontcare;
				}

				bool DoApplyValue(TProperty *pState, const char *szName, bool bValue)
				{
					bool bUnique = GetAtomValue(pState, szName) != bValue;

					TProperty hCondition;

					DoClear(&hCondition);

					if (szName != NULL)
					{
						SetAtomValue(&hCondition, szName, bValue);
					}

					DoApply(pState, &hCondition);

					return bUnique;
				}


				//
				// Other
				// 

				bool ValidateIndex(int iIdx, int uLimit)
				{
					return iIdx >= 0 && iIdx < uLimit;
				}

			};
		}
	}
}


#undef GetAtom
#undef SetAtom
#undef HasAtom
#undef DoCareAtom
#undef DoAtom
#undef ToString

#endif