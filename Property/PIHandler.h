#ifndef _GOAP_IPROPERTY_HANDLER_H_
#define _GOAP_IPROPERTY_HANDLER_H_

#include <utils-cpp/XItem.h>
#include <utils-cpp/IComponentBase.h>
#include <utils-cpp/XComponentBase.h>
#include <utils-cpp/XDelegate.h>

#include "../Action/AIBase.h"

namespace GOAP
{

	namespace Balance
	{
		namespace Property
		{
			class XSet;
		}
	}

	namespace Property
	{
		class XSet;
		class IHandler : public Utils::XItem, public Utils::Component::IBase
		{
		public:
			/*
			Property Printing/Logging information
			*/
		
			typedef Utils::Delegate::X3<void, int, const char *, bool> TDelegate;

			virtual void Print(TDelegate & hCallback) = 0;


			/*
			Property value accessors
			*/

			virtual bool InitValue(const char *szName, bool bValue) = 0;

			virtual bool SetValue(const char *szName, bool bValue) = 0;

			virtual bool GetValue(const char *szName) = 0;

			virtual void IncludeMask(void *pMask) = 0;
			
			virtual void Apply(Action::IBase::EConditionFlags eFlags, Action::IBase *pState, Action::IBase *pAction, const char *szName = NULL) = 0;

			virtual bool Match(Action::IBase::EConditionFlags eFlags, Action::IBase *pState, Action::IBase *pAction, bool bFromCare = false) = 0;

			/*
			Property cleaning
			*/

			virtual void Clear() = 0;

			virtual void Clear(Action::IBase *pAction) = 0;

			/*
			Action creation & property accessor within pAction by eFlags
			*/

			virtual Property::IHandler* operator() (Action::IBase *pAction, Action::IBase::EConditionFlags eFlags) = 0;

			virtual void InitializeAction(Action::IBase *pAction) = 0;
			
			virtual Action::IBase *CreateState(const char *szName) = 0;

			Property::IHandler* operator() (Action::IBase *pAction, bool bPreCondition);

			/*
			Component methods
			*/
			void Initialize(Utils::Component::XBase *entity);

			void Update(Utils::Component::XBase *entity);

			void Shutdown(Utils::Component::XBase *entity);

			void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args);

			void EnableBalancer();

			void DisableBalancer();


		protected:

			typedef Utils::Delegate::X3<int, bool, int, bool> TAtomCostDelegate;

			int GetCost(bool bPreCondition, const char *szName, bool bValue);

			int GetCost(bool bPreCondition, int iAtomIndex, bool bValue);

			int GetNormalCost(bool bPreCondition, int iAtomIndex, bool bValue);
			
			int GetBalancedCost(bool bPreCondition, int iAtomIndex, bool bValue);

			bool						m_bUseBalancer;
			TAtomCostDelegate			m_hAtomCost[2];

			XSet *						m_pContext; // < Properties szName context
			Balance::Property::XSet *	m_pBalance; // < Properties balance cost set

		};
	}
}

#endif