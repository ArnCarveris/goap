#ifndef _GOAP_TEST_H_
#define _GOAP_TEST_H_

#include <utils-cpp/XComponentBase.h>
#include <utils-cpp/XDictionary.h>
#include <utils-cpp/XPlannerAStar.h>

#include <GOAP/Core/CXBlackBoard.h>
#include <GOAP/Core/CXBase.h>
#include <GOAP/Action.h>
#include <GOAP/Property/PXManager.h>

#define USE_BIT_FIELD_HANDLE

#ifdef USE_BIT_FIELD_HANDLE 
//
// Quite fast, using structure of two primitive data types, total context unique atom count is limited to type bit size, has fixed size
//

#define CONTEXT_LIMIT 64 // this is bit size of LLI data type

#include <GOAP/Property/BitField/PBFXHandler.h>

typedef GOAP::Property::BitField::XHandler<long long int> XHandler;
typedef XHandler::TProperty TProperty;

#else 
//
// Due unoptimized naiive implementation of utils-cpp dictionary (<int, bool>) is kind'a slow for now, but it has unlimited context size, has dynamic size
//

#define CONTEXT_LIMIT 0

#include <GOAP/Property/Dictionary/PDXHandler.h>

typedef GOAP::Property::Dictionary::XHandler XHandler;
typedef GOAP::Property::Dictionary::XBase TProperty;

#endif

#define named00(_name_) (false, _name_, false)
#define named01(_name_) (false, _name_, true)
#define named10(_name_) (true, _name_, false)
#define named11(_name_) (true, _name_, true)

//
// Test data classes
//

namespace Test
{
	typedef GOAP::Action::XStatic<TProperty> XAction;
	typedef long long TMask;

	typedef Utils::Component::XBase XEntity;

	XEntity *Entity(bool bDelete = false)
	{
		static XEntity *entity = NULL;

		if (entity == NULL)
		{
			entity = new XEntity();
		}
		else if (bDelete)
		{
			entity->Remove();
		}

		return entity;
	}

	void Shutdown()
	{
		XEntity *entity = Entity(false);

		entity->Shutdown(NULL);

		Entity(true);
	}

	namespace Component
	{
		struct XAction : Utils::Component::IBase
		{
			GOAP::Action::XSet *		set;
			GOAP::Action::IBase *		action;
			GOAP::Action::XManager *	manager;

			void Initialize(XEntity *entity)
			{
				action = NULL;

				manager = GOAP::Core::BlackBoard.Get<GOAP::Action::XManager>();
				set = manager->GetNamedSet<GOAP::Action::XSet>("test");

				entity->Include<GOAP::Action::XSet>(set);
			}

			void Update(XEntity *entity) { }

			void Shutdown(XEntity *entity)
			{
				manager->Clear();
			}
		};

		struct XProperty : Utils::Component::IBase
		{
			GOAP::Property::IHandler *	handler;
			GOAP::Property::XManager *	manager;

			void Initialize(XEntity *entity)
			{
				manager = GOAP::Core::BlackBoard.Get<GOAP::Property::XManager>();

				entity->Include<GOAP::Property::XSet>(manager->GetNamedSet("test", CONTEXT_LIMIT));

				handler = entity->Create<GOAP::Property::IHandler, XHandler>();

			}

			void Update(XEntity *entity) { }

			void Shutdown(XEntity *entity)
			{
				manager->Clear();

				handler = NULL;
			}
		};

		struct XBase : Utils::Component::IBase
		{
			Utils::Planner::IBase *		planner;
			GOAP::Core::IBase *			base;

			void Initialize(XEntity *entity)
			{
				planner = entity->Create < Utils::Planner::IBase, Utils::Planner::XAStar >();

				base = entity->Create < GOAP::Core::IBase, GOAP::Core::XBase < TProperty > >();
			}

			void Update(XEntity *entity) { }

			void Shutdown(XEntity *entity)
			{
				planner = NULL;
				base = NULL;
			}
		}; 
		
		struct XCore : Utils::Component::IBase
		{
			XBase *b;
			XAction *a;
			XProperty *p;

			XCore()
			{
				b = NULL;
				a = NULL;
				p = NULL;
			}

			void Initialize(XEntity *entity)
			{
				a = entity->Create<XAction>();
				p = entity->Create<XProperty>();
				b = entity->Create<XBase>();
			}

			void Update(XEntity *entity) { }

			void Shutdown(XEntity *entity)
			{
				b = NULL;
				a = NULL;
				p = NULL;
			}

			void InitValue(bool bPreCondition, const char *name, bool value)
			{
				(*p->handler)(a->action, bPreCondition)->SetValue(name, value);
			}

			void CreateAction(const char *name)
			{
				a->action = GOAP::Action::Create<Test::XAction>(p->handler, name, a->set);
				
				InitValue named11("alive");
			}

			void CreateState(const char *name)
			{
				a->action = GOAP::Action::CreateState<TProperty>(p->handler, name);
			}
		};
	}
}

#endif