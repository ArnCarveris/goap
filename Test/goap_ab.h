#ifndef _GOAP_AB_H_
#define _GOAP_AB_H_

#include "test.h"
#include "description.h"


void goap_ab()
{

	Test::XEntity *entity = Test::Entity();

	Test::Component::XCore *c = entity->Create<Test::Component::XCore>();

	entity->Create <Test::Component::Description::XProperty>();
	entity->Create <Test::Component::Description::XAction>();
	entity->Create <Test::Component::Description::XBase>();

	entity->Initialize(NULL);

	c->CreateAction("Scout");
	c->InitValue named11("armedwithgun");
	c->InitValue named01("enemyvisible");

	c->CreateAction("Approach");
	c->InitValue named11("enemyvisible");
	c->InitValue named01("nearenemy");

	c->CreateAction("Aim");
	c->InitValue named11("enemyvisible");
	c->InitValue named11("weaponloaded");
	c->InitValue named01("enemylinedup");

	c->CreateAction("Shoot");
	c->InitValue named11("enemylinedup");
	c->InitValue named00("enemyalive");

	c->CreateAction("Load");
	c->InitValue named11("armedwithgun");
	c->InitValue named01("weaponloaded");

	c->CreateAction("Detonatebomb");
	c->InitValue named11("armedwithbomb");
	c->InitValue named11("nearenemy");
	c->InitValue named00("alive");
	c->InitValue named00("enemyalive");

	c->CreateAction("Flee");
	c->InitValue named11("enemyvisible");
	c->InitValue named00("nearenemy");


	c->CreateState("current.goal");
	c->InitValue named10("enemyvisible");
	c->InitValue named11("alive");
	c->InitValue named11("armedwithgun");
	c->InitValue named10("weaponloaded");
	c->InitValue named10("enemylinedup");
	c->InitValue named11("enemyalive");
	c->InitValue named11("armedwithbomb");
	c->InitValue named10("nearenemy");
	
	c->InitValue named00("enemyalive");
	c->InitValue named01("alive"); // add this to avoid suicide actions in plan.

	entity->Update(NULL);

	entity->Get<Test::Component::Description::XAction>()->PrintSet();

	printf("\n");
	
	entity->Get<Test::Component::Description::XAction>()->Print();

	printf("\n\n\t[Planning...]\n");

	const int cost = c->b->base->MakePlan(c->a->action);

	entity->Get<Test::Component::Description::XBase>()->Print(cost);

	Test::Shutdown();
}

#endif