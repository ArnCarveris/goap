#ifndef _GOAP_ABDM_H_
#define _GOAP_ABDM_H_

#include "goap_absm.h"

#include <GOAP/Decision/DXMachine.h>
#include <GOAP/Decision/DXManager.h>

namespace Test
{
	namespace Component
	{
		struct XDecision : Utils::Component::IBase
		{
			GOAP::Decision::XSet *set;
			GOAP::Decision::INode *node;
			GOAP::Decision::XMachine *machine;
			GOAP::Decision::XManager *manager;

			void Initialize(XEntity *entity)
			{
				node = NULL;

				manager = GOAP::Core::BlackBoard.Get<GOAP::Decision::XManager>();

				set = manager->GetNamedSet < GOAP::Decision::XSet>("test");

				entity->Include<GOAP::Decision::XSet>(set);

				machine = entity->Create<GOAP::Decision::XMachine>();
			}

			void Update(XEntity *entity) { }
			
			void Shutdown(XEntity *entity) { }

			T(Node) void CreateNode(const char *name)
			{
				node = GOAP::Decision::Create<TNode>(name, set);
			}

			T(Node) void CreateNode(XEntity *entity, const char *name)
			{
				node = GOAP::Decision::Create<TNode>(entity, name);
			}
		};
	}

	
}

namespace DecisionNode
{
	class XEnemyHealthCare : public GOAP::Decision::INode
	{
	public:

		float CalculateUtility()
		{
			return -1.0f;
		}
	};

	class XEnemyDistace : public GOAP::Decision::INode
	{
	public:

		float CalculateUtility()
		{
			return -1.0f;
		}
	};

	class XHealthCare : public GOAP::Decision::INode
	{
	public:

		float CalculateUtility()
		{
			return 1.0f;
		}
	};
}


#define RegisterDecisionX(_class_, _name_) d->CreateNode<DecisionNode::X##_class_>(entity, _name_)

#define Set(_name_, _value_) (*d->machine)(_name_, _value_);
#define Set0(_name_) Set(_name_, false)
#define Set1(_name_) Set(_name_, true)


void goap_abdm()
{
	Test::XEntity *entity = Test::Entity();


	Test::Component::XState *s = entity->Create <Test::Component::XState>();
	Test::Component::XDecision *d = entity->Create<Test::Component::XDecision>();

	entity->Create <Test::Component::Description::XProperty>();
	entity->Create <Test::Component::Description::XState>();
	entity->Create <Test::Component::XCore>();

	entity->Initialize(NULL);

	RegisterX(Scout);
	RegisterX(Approach);
	RegisterX(Aim);
	RegisterX(Shoot);
	RegisterX(Load);
	RegisterX(DetonateBomb);
	RegisterX(Flee);
	RegisterX(BlowUpEnemy);
	RegisterX(PickUp);
	RegisterX(MakeFriend);

	RegisterDecisionX(EnemyHealthCare, "enemyalive");
	RegisterDecisionX(EnemyDistace, "nearenemy");
	RegisterDecisionX(HealthCare, "alive");

	entity->Update(NULL);

	Set0("enemyvisible");
	Set1("armedwithgun");
	Set0("weaponloaded");
	Set0("enemylinedup");
	Set1("enemyalive");
	Set1("armedwithbomb");
	Set0("nearenemy");
	Set1("alive");


	d->machine->Update(1.0f);
	
	entity->Get<Test::Component::Description::XState>()->Print();

	entity->Shutdown(NULL);
}

#undef RegisterDecisionX
#undef RegisterX
#undef Set
#undef Set0
#undef Set1

#endif