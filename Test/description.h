#ifndef _GOAP_DESCRIPTION_H_
#define _GOAP_DESCRIPTION_H_

#include "test.h"

#include <GOAP/Action/AXBase.h>

namespace Test
{
	namespace Component
	{
		namespace Description
		{
			class XProperty : public Utils::Component::IBase
			{
			public:

				enum EPrintLayout
				{
					State = 0,
					PostCondition = 1,
					PreCondition = 2,
					None = 3,
				};

				void Initialize(XEntity *entity)
				{
					manager = GOAP::Core::BlackBoard.Get < GOAP::Action::XManager >();

					entity->RegisterMethod<XProperty, &XProperty::OnInitialize >("OnInitialize", this);
				}

				void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
				{
					handler = entity->Require<GOAP::Property::IHandler>();
				}

				void Update(XEntity *entity) {}

				void Shutdown(XEntity *entity)
				{
					handler = NULL;
				}

				void Print(EPrintLayout layout)
				{
					handler->Print(get_callback(layout));
				}

				void Print(GOAP::Action::IBase *action)
				{
					printf("%s:\n", action->GetName());

					(*handler)(action, GOAP::Action::IBase::EConditionFlags::Pre)->Print(get_callback(EPrintLayout::PreCondition));
					(*handler)(action, GOAP::Action::IBase::EConditionFlags::Post)->Print(get_callback(EPrintLayout::PostCondition));
				}

				void Print(GOAP::Action::XSet *set)
				{
					for (int i = 0, c = set->GetSize(); i < c; ++i)
					{
						Print(manager->GetByID((*set)[i]));
					}
				}

			protected:
				static void print_state(int index, const char *name, bool value)
				{
					char upper[128];
					size_t j;

					for (j = 0; j < strlen(name); ++j)
					{
						upper[j] = (name[j] - 32);
					}

					upper[j++] = 0;

					printf("%s,", value ? upper : name);
				}

				static void print_precondition(int index, const char *name, bool value)
				{
					printf("  %s==%d\n", name, value);
				}

				static void print_postcondition(int index, const char *name, bool value)
				{
					printf("  %s:=%d\n", name, value);
				}
				
				static GOAP::Property::IHandler::TDelegate& get_callback(EPrintLayout layout)
				{
					static GOAP::Property::IHandler::TDelegate callbacks[] =
					{
						GOAP::Property::IHandler::TDelegate::FromFunction<print_state>(),
						GOAP::Property::IHandler::TDelegate::FromFunction<print_postcondition>(),
						GOAP::Property::IHandler::TDelegate::FromFunction<print_precondition>(),
						GOAP::Property::IHandler::TDelegate::FromFunction<print_state>()
					};

					return callbacks[(int)layout];
				}

				GOAP::Property::IHandler *handler;
				GOAP::Action::XManager *manager;
			};

			struct XAction : Utils::Component::IBase
			{
				XProperty *p;

				Component::XAction *a;

				void Initialize(XEntity *entity)
				{
					entity->RegisterMethod<XAction, &XAction::OnInitialize >("OnInitialize", this);
				}

				void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
				{
					a = entity->Require <Component::XAction>();
					p = entity->Require <XProperty>();
				}

				void Update(XEntity *entity) {}

				void Shutdown(XEntity *entity)
				{
					a = NULL;
					p = NULL;
				}

				void PrintSet()
				{
					p->Print(a->set);
				}

				void Print()
				{
					p->Print(a->action);
				}
			};

			struct XBase : Utils::Component::IBase
			{
				GOAP::Action::IBase *action;
				Component::XBase *b;
				
				void Initialize(XEntity *entity)
				{
					action = NULL;

					entity->RegisterMethod<XBase, &XBase::OnInitialize >("OnInitialize", this);
				}

				void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
				{
					b = entity->Require<Component::XBase>();
				}

				void Update(XEntity *entity) { }

				void Shutdown(XEntity *entity)
				{
					action = NULL;
					b = NULL;
				}

				void Print(int cost)
				{
					printf("\n\nPlan [Cost = %d, Length = %d]\n", cost,	b->base->GetPlanLength());

					for (int i = 0, l = b->base->GetPlanLength(); i<l; ++i)
					{
						action = b->base->GetPlannedActionAt(i);

						printf("\n%d: %-20s", i, action->GetName());
					}

				}
			};
		}
	}
}

#endif