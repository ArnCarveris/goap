#ifndef _GOAP_ABSM_H_
#define _GOAP_ABSM_H_

#include "test.h"

#include <GOAP/State/SXMachine.h>
#include <GOAP/State/SXVerifier.h>

namespace Test
{
	namespace Component
	{
		struct XState : Utils::Component::IBase
		{
			GOAP::State::XMachine *machine;
			GOAP::State::XBase *state;

			Utils::State::XSet *set;
			Utils::State::XManager *manager;

			Component::XCore *c;

			void Initialize(XEntity *entity)
			{
				manager = Utils::State::XManager::GetInstance();

				set = manager->GetNamedSet < Utils::State::XSet >("test");
				machine = entity->Create<GOAP::State::XMachine>();

				entity->Create < GOAP::State::XValidator<TMask> >();
				entity->Create < GOAP::State::IVerifier, GOAP::State::XVerifier<TMask> >();
				entity->Include<Utils::State::XSet>(set);

				entity->RegisterMethod<XState, &XState::OnInitialize >("OnInitialize", this);
			}

			void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
			{
				c = entity->Require <Component::XCore>();
			}

			void Update(XEntity *entity) { }

			void Shutdown(XEntity *entity)
			{
				manager->Clear();

				machine = NULL;
			}

			T(State) void CreateState(const char *name)
			{
				state = GOAP::State::Create<TState>(name, set);
			}

			T(State) void CreateState(XEntity *entity, const char *name)
			{
				state = GOAP::State::Create<TState>(entity, name);

				state->RebindOwner();

				((TState*)state)->OnInit(entity);
			}
		};

		namespace Description
		{
			struct XState : Utils::Component::IBase
			{
				XProperty *p;
				GOAP::State::XMachine *machine;

				void Initialize(XEntity *entity)
				{
					entity->RegisterMethod<XState, &XState::OnInitialize >("OnInitialize", this);
				}

				void OnInitialize(Utils::Component::XBase *entity, Utils::Component::XBase::IEventArgs *args)
				{
					machine = entity->Require<GOAP::State::XMachine>();
					p = entity->Require<XProperty>();
				}

				void Update(XEntity *entity)
				{
					machine = entity->Get<GOAP::State::XMachine>();
				}

				void Shutdown(XEntity *entity)
				{
					machine = NULL;
				}

				void PrintState(GOAP::State::XMachine::EState state, const char *title = "\n[:")
				{
					if (title != NULL)
					{
						printf(title);
					}

					(*machine)(state); p->Print(XProperty::EPrintLayout::State);
				}

				void PrintState(GOAP::State::XBase *state, int id)
				{
					if (id < 1)
					{
						printf("\n~========================~");
					}

					printf("\n\nstatus[%s].current_state[%d] = %s\n", GOAP::State::XMachine::GetStatusName(machine->GetStatus()), id, state == NULL ? "null" : state->GetName());
				}

				void Print()
				{

					do
					{

						GOAP::State::XBase *current = (GOAP::State::XBase*)(*machine)[NULL];
						
						PrintState(current, 0);

						if (current == NULL)
						{
							machine->GoToNextState();
						}
						else
						{
							PrintState(GOAP::State::XMachine::EState::Current);
							
							machine->Update(0.0f);

							PrintState(GOAP::State::XMachine::EState::Current);
						}

						current = (GOAP::State::XBase*)(*machine)[NULL];

						PrintState(current, 1);
						
						getchar();

					} while (machine->GetStatus() != GOAP::State::XMachine::Done);
				}
			};
		}
	}

	
}

#define X(_class_, _constr_, _destr_, _init_, _update_) class X##_class_ : public XDefault {public: X##_class_() _constr_ ~ X##_class_() _destr_ void OnUpdate(Utils::State::XMachine *machine, float fDeltaTime) _update_ protected: void InitializeAction()  _init_ }

namespace MachineState
{
	class XDefault : public GOAP::State::XBase
	{
	public:
		void OnInit(Test::XEntity *entity)
		{
			CreateAction<Test::XAction>();

			InitValue named11("alive");

			InitializeAction();
		}

		bool OnUnRegister() { return true; }
		bool OnRegister() { return true; }

		bool OnEnter() { return true; }
		bool OnExit() { return true; }

	protected:		

		virtual void InitializeAction() = 0;
	};

	X(Scout, {}, {}, {
		InitValue named11("armedwithgun");
		InitValue named01("enemyvisible");
	}, {
		ApplyValue("enemyvisible");
	});

	X(Approach, {}, {}, {
		InitValue named11("enemyvisible");
		InitValue named01("nearenemy");
	}, {
		ApplyValue("nearenemy");
	});

	X(Aim, {}, {}, {
		InitValue named11("enemyvisible");
		InitValue named11("weaponloaded");
		InitValue named01("enemylinedup");
	}, {
		ApplyValue("enemylinedup");
	});

	X(Shoot, {}, {}, {
		InitValue named11("enemylinedup");
		InitValue named00("enemyalive");
	}, {
		ApplyValue("enemyalive");
	});

	X(Load, {}, {}, {
		InitValue named11("armedwithgun");
		InitValue named01("weaponloaded");
	}, {
		ApplyValue("weaponloaded");
	});


	X(DetonateBomb, {}, {}, {
		InitValue named11("armedwithbomb");
		InitValue named10("nearenemy");
		InitValue named00("alive");
	}, {
		ApplyValue("alive");
	});

	X(BlowUpEnemy, {}, {}, {
		InitValue named11("armedwithbomb");
		InitValue named11("nearenemy");
		InitValue named00("alive");
		InitValue named00("enemyalive");
	}, {
		ApplyValue("alive");
		ApplyValue("enemyalive");
	});

	X(Flee, {}, {}, {
		InitValue named11("enemyvisible");
		InitValue named00("nearenemy");
	}, {
		ApplyValue("nearenemy");
	});


	X(PickUp, {}, {}, {
		InitValue named10("armedwithgun");
		InitValue named01("armedwithgun");
	}, {
		ApplyValue("armedwithgun");
	});

	X(MakeFriend, {}, {}, {
		InitValue named11("enemyalive");
		InitValue named10("nearenemy");
		InitValue named01("nearenemy");
	}, {
		ApplyValue("nearenemy");
	});
}

#undef X


#define RegisterX(_class_) s->CreateState<MachineState::X##_class_ >(entity, #_class_)
#define Set(_state_, _name_, _value_) (*s->machine)(GOAP::State::XMachine::EState::##_state_, _name_, _value_)

#define Set00(_name_) Set(Current, _name_, false)
#define Set01(_name_) Set(Current, _name_, true)
#define Set10(_name_) Set(Goal, _name_, false)
#define Set11(_name_) Set(Goal, _name_, true)

void goap_absm()
{


	Test::XEntity *entity = Test::Entity();

	Test::Component::XState *s = entity->Create<Test::Component::XState>();

	entity->Create <Test::Component::XCore>();

	entity->Create <Test::Component::Description::XProperty>();
	entity->Create <Test::Component::Description::XState>();

	entity->Initialize(NULL);

	
	RegisterX(Scout);
	RegisterX(Approach);
	RegisterX(Aim);
	RegisterX(Shoot);
	RegisterX(Load);
	RegisterX(DetonateBomb);
	RegisterX(Flee);
	RegisterX(BlowUpEnemy);
	RegisterX(PickUp);
	RegisterX(MakeFriend);
	
	Set00("enemyvisible");
	Set01("armedwithgun");
	Set00("weaponloaded");
	Set00("enemylinedup");
	Set01("enemyalive");
	Set01("armedwithbomb");
	Set00("nearenemy");
	Set01("alive");

	Set10("enemyalive");
	Set11("alive"); // add this to avoid suicide actions in plan.

	entity->Update(NULL);

	s->machine->MakePlan();
	
	entity->Get<Test::Component::Description::XState>()->Print();

	entity->Shutdown(NULL);
}

#undef Set00
#undef Set01
#undef Set10
#undef Set11
#undef Set

#endif