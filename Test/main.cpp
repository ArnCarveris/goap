
#include <string.h>
#include <stdio.h>

#include <utils-cpp/XDelegateStageTester.h>
#include <GOAP/Core/CXBlackBoard.h>

#include "goap_ab.h"
#include "goap_absm.h"
#include "goap_abdm.h"

//
// CONTEXT_LIMIT, is total atoms count limit to be handle, 0 - means unlimited
//

int main(int argc, char* argv[])
{
	GOAP::Core::BlackBoard.Initialize();

#define test_list \
	{ \
	{ "action-based", goap_ab }, \
	{ "a-b-state-machine", goap_absm }, \
	{ "a-b-decision-machine", goap_abdm }\
	}
	UtilsCreateXDelegateStageTester(tester, "\n\n\t\t[GOAP '%s' Implementation]\n");

	tester->Run();

	delete tester;

#undef test_list

	GOAP::Core::BlackBoard.Shutdown();

	return 0;
}

